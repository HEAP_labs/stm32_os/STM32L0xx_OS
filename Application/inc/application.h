/**
 * @file    ./inc/application.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the application module
 */

#ifndef _APPLICATION_H
#define _APPLICATION_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/*!
  * @brief Baudrate definitions
*/
#define USART_BAUDRATE  ((uint32_t)9600)
#define LPUART_BAUDRATE ((uint32_t)9600)

/*!
  * @brief Portpin definitions
*/
#define USART_PORT_PIN_RX ((API_GPIO_type_t *)&PA15)
#define USART_PORT_PIN_TX ((API_GPIO_type_t *)&PA2)
#define USART_ALTERNATIVE_FUNCTION_RX ((API_GPIO_alternative_function_t) ALTERNATIVE_FUNCTION_4)
#define USART_ALTERNATIVE_FUNCTION_TX ((API_GPIO_alternative_function_t) ALTERNATIVE_FUNCTION_4)


#define LPUART_PORT_PIN_RX ((API_GPIO_type_t *)&PA13)
#define LPUART_PORT_PIN_TX ((API_GPIO_type_t *)&PA2)
#define LPUART_ALTERNATIVE_FUNCTION_RX ((API_GPIO_alternative_function_t) ALTERNATIVE_FUNCTION_6)
#define LPUART_ALTERNATIVE_FUNCTION_TX ((API_GPIO_alternative_function_t) ALTERNATIVE_FUNCTION_6)


/*!
  * @brief measurement_state_machine period time [s]
*/
#define T_PERIOD_S ((double)1e-3)

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* _APPLICATION_H */

