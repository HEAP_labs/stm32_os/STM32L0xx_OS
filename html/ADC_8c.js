var ADC_8c =
[
    [ "VDDA", "ADC_8c.html#a2d52976aedaedf74a90019a689170620", null ],
    [ "ADC_Calibrate", "ADC_8c.html#a7eb6744d5738bf35b1cb86b44cdc99ad", null ],
    [ "ADC_Disable", "ADC_8c.html#aa256af5b8a253bdc3f6aacaa29830e64", null ],
    [ "ADC_Enable", "ADC_8c.html#a4267de5bc303060b4dfd2997c078c602", null ],
    [ "ADC_GetVoltage", "ADC_8c.html#a8136e67cca9eae124cf49bca2378dcf3", null ],
    [ "ADC_Init", "ADC_8c.html#a3fdaf3b8949082abe85a7e7083087eb7", null ],
    [ "p_adc_end_of_conversion_handler", "ADC_8c.html#af25852d7314929ee34e273d856cf15de", null ]
];