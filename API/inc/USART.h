/**
 * @file    ./API/inc/USART.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @date    29.08.2016
 * @version 1.0
 * @brief   Header file for the USART.c module
 */

#ifndef __USART_H
#define __USART_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_USART_DMA_init(uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t priority);
extern void API_USART_init(API_GPIO_type_t * usart_port_pin_rx,
  API_GPIO_alternative_function_t usart_alternative_function_rx,
  API_GPIO_type_t * usart_port_pin_tx,
  API_GPIO_alternative_function_t usart_alternative_function_tx,
  uint32_t baudrate);
extern void API_USART_DMA_send(uint8_t tx_length);
extern void API_USART_DMA_receive(uint16_t rx_length);

#endif /* __USART_H */

