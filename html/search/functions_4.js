var searchData=
[
  ['get_5ferror_5fcode',['get_error_code',['../application_8c.html#ad85f4406496b593af05dbaed475462e9',1,'application.c']]],
  ['get_5fplate_5fstate',['get_plate_state',['../application_8c.html#a36dcfc6fee22b0974badca8dff09e55d',1,'application.c']]],
  ['getdi',['GetDI',['../GPIO_8c.html#a86c036aa80b88e113e5783aa9f06a2b3',1,'GetDI(GPIOInstance_t *GPIOInstance):&#160;GPIO.c'],['../GPIO_8h.html#a86c036aa80b88e113e5783aa9f06a2b3',1,'GetDI(GPIOInstance_t *GPIOInstance):&#160;GPIO.c']]],
  ['gpio_5fconfig',['gpio_config',['../application_8c.html#a16ab82c8a753266e4ffede309ca4259c',1,'application.c']]],
  ['gpio_5fdeinit',['GPIO_DeInit',['../GPIO_8c.html#a55274c05fa9e816b7d0567799f2adda3',1,'GPIO_DeInit(GPIOInstance_t *GPIOInstance):&#160;GPIO.c'],['../GPIO_8h.html#a55274c05fa9e816b7d0567799f2adda3',1,'GPIO_DeInit(GPIOInstance_t *GPIOInstance):&#160;GPIO.c']]],
  ['gpio_5finit',['GPIO_Init',['../GPIO_8c.html#a885c93e9837729510e85129e4ac82a86',1,'GPIO_Init(GPIOInstance_t *GPIOInstance):&#160;GPIO.c'],['../GPIO_8h.html#a885c93e9837729510e85129e4ac82a86',1,'GPIO_Init(GPIOInstance_t *GPIOInstance):&#160;GPIO.c']]]
];
