var searchData=
[
  ['adc_5fcalibrate',['ADC_Calibrate',['../ADC_8c.html#a7eb6744d5738bf35b1cb86b44cdc99ad',1,'ADC.c']]],
  ['adc_5fdisable',['ADC_Disable',['../ADC_8c.html#aa256af5b8a253bdc3f6aacaa29830e64',1,'ADC.c']]],
  ['adc_5fenable',['ADC_Enable',['../ADC_8c.html#a4267de5bc303060b4dfd2997c078c602',1,'ADC.c']]],
  ['adc_5fgetvoltage',['ADC_GetVoltage',['../ADC_8c.html#a8136e67cca9eae124cf49bca2378dcf3',1,'ADC_GetVoltage(GPIOInstance_t *adc_pin):&#160;ADC.c'],['../ADC_8h.html#a8136e67cca9eae124cf49bca2378dcf3',1,'ADC_GetVoltage(GPIOInstance_t *adc_pin):&#160;ADC.c']]],
  ['adc_5finit',['ADC_Init',['../ADC_8c.html#a3fdaf3b8949082abe85a7e7083087eb7',1,'ADC_Init():&#160;ADC.c'],['../ADC_8h.html#a5137b551f1b83b0f4d8df7d071a3d3a6',1,'ADC_Init(void):&#160;ADC.c']]]
];
