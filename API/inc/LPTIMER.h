/**
 * @file    ./API/inc/LPTIMER.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the LPTIMER.c module
 */

#ifndef __LPTIMER_H
#define __LPTIMER_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void API_LPTIMER_start(void);
void API_LPTIMER_init(void (*handler)(double t_period_s), double t_period_s);

#endif /* __LPTIMER_H */

