/**
 * @file    ./API/src/LPTIMER.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the LPTIMER module
 */

#include "PerUsings.h"
#ifdef __USING_LPTIMER

/* Includes ------------------------------------------------------------------*/
#include "LPTIMER.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/*! @brief function pointer for the capture-compare handler */
static void (*p_lptimer_ccp_handler)();

/*! @brief period of the timer in seconds */
static double g_t_period_s;

/* Private function prototypes -----------------------------------------------*/
void API_LPTIMER_start(void);
void API_LPTIMER_init(void (*handler)(double t_period_s), double t_period_s);
void LPTIM1_IRQHandler(void);

/* Private functions ---------------------------------------------------------*/

/*!
 * @brief Initialize the Low-Power Timer
 * @param handler pointer to the autoreload match interrupt handler
 * @param t_period_s the period of the timer to the autoreload match in seconds
 */
void API_LPTIMER_init(void (*handler)(double t_period_s), double t_period_s) {
    /* store the address of the handler */
    p_lptimer_ccp_handler = handler;
    /* store the timer period */
    g_t_period_s = t_period_s;

    /* Enable the peripheral clock of LPTimer */
    RCC->APB1ENR |= RCC_APB1ENR_LPTIM1EN;

    /* Configure the timer-prescaler */
    LPTIM1->CFGR = LPTIM_CFGR_PRESC_0;
    /* Enable the Autoreload match interrupt */
    LPTIM1->IER |= LPTIM_IER_ARRMIE;
    /* Enable LPTimer  */
    LPTIM1->CR |= LPTIM_CR_ENABLE;
    /* Set the Autoreload-Register, autorelaod - match interrupt is generated at t_period_s */
    LPTIM1->ARR = (uint16_t) (t_period_s * SystemCoreClock / 2.0) - 1;

    /* Enable Interrupt on LPTIM1 */
    NVIC_EnableIRQ(LPTIM1_IRQn);
    /* Set priority for LPTIM1 */
    NVIC_SetPriority(LPTIM1_IRQn, 3);
}

/*!
 * @brief Start the Low-Power Timer
 */
void API_LPTIMER_start(void) {
    LPTIM1->CR |= LPTIM_CR_SNGSTRT;
}

/*!
  * @brief Interrupt handler for the Low-Power-Timer
  * @param  None
  * @retval None
 */
void LPTIM1_IRQHandler(void) {
    if ((LPTIM1->ISR & LPTIM_ISR_ARRM) != 0) /* Check ARR match */
    {
        LPTIM1->ICR |= LPTIM_ICR_ARRMCF; /* Clear ARR match flag */
        /* call the handler */
        (*p_lptimer_ccp_handler)(g_t_period_s);
    }
}

#endif // __USING_LPTIMER
