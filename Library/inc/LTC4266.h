/**
 * @file    ./Library/inc/LTC4266.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file for the LTC4266.c Library
 */

#ifndef __LTC4266_H
#define __LTC4266_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "I2C.h"

/* Exported types ------------------------------------------------------------*/
typedef enum ltc4266_output_mode_e
{
    PASSIVE,
    TYPE_1,
    TYPE_2,
    LTPOE_PP,
} ltc4266_output_mode_t;

typedef enum ltc4266_output_state_e
{
  OFF=1,
  ON,
} ltc4266_output_state_t;

typedef struct ltc4266_config_s
{
    ltc4266_output_state_t output_enable;
    ltc4266_output_mode_t output_mode;
    double current_limit; // 0..default current from output mode
    double current;
} ltc4266_config_t;

/* Exported constants --------------------------------------------------------*/
/*** Interrupts ***/
#define intstat ((uint8_t)0x00)
#define intmask ((uint8_t)0x01)

/*** Events ***/
#define pwrevn      ((uint8_t)0x02)
#define pwrevn_cor  ((uint8_t)0x03)
#define detevn      ((uint8_t)0x04)
#define detevn_cor  ((uint8_t)0x05)
#define fltevn      ((uint8_t)0x06)
#define fltevn_cor  ((uint8_t)0x07)
#define tsevn       ((uint8_t)0x08)
#define tsevn_cor   ((uint8_t)0x09)
#define supevn      ((uint8_t)0x0A)
#define supevn_cor  ((uint8_t)0x0B)

/*** Status ***/
#define statp1  ((uint8_t)0x0C)
#define statp2  ((uint8_t)0x0D)
#define statp3  ((uint8_t)0x0E)
#define statp4  ((uint8_t)0x0F)
#define statpwr ((uint8_t)0x10)
#define statpin ((uint8_t)0x11)

/*** Configuration ***/
#define opmd    ((uint8_t)0x12)
#define disena  ((uint8_t)0x13)
#define detena  ((uint8_t)0x14)
#define midspan ((uint8_t)0x15)
#define mconf   ((uint8_t)0x17)

/*** Pushbuttons ***/
#define detpb ((uint8_t)0x18)
#define pwrpb ((uint8_t)0x19)
#define rstpb ((uint8_t)0x1A)

/*** Identification ***/
#define id ((uint8_t)0x1B)

/*** Limit Timing ***/
#define tlim12 ((uint8_t)0x1E)
#define tlim34 ((uint8_t)0x1F)

/*** Port Parametric Measurement ***/
#define ip1lsb  ((uint8_t)0x30)
#define ip1msb  ((uint8_t)0x31)
#define vp1lsb  ((uint8_t)0x32)
#define vp1msb  ((uint8_t)0x33)
#define ip2lsb  ((uint8_t)0x34)
#define ip2msb  ((uint8_t)0x35)
#define vp2lsb  ((uint8_t)0x36)
#define vp2msb  ((uint8_t)0x37)
#define ip3lsb  ((uint8_t)0x38)
#define ip3msb  ((uint8_t)0x39)
#define vp3lsb  ((uint8_t)0x3A)
#define vp3msb  ((uint8_t)0x3B)
#define ip4lsb  ((uint8_t)0x3C)
#define ip4msb  ((uint8_t)0x3D)
#define vp4lsb  ((uint8_t)0x3E)
#define vp4msb  ((uint8_t)0x3F)

/*** Configuration 1 ***/
#define firmware  ((uint8_t)0x41)
#define wdog      ((uint8_t)0x42)
#define devide    ((uint8_t)0x43)

/*** High Power Features, Global ***/
#define hpen    ((uint8_t)0x44)

/*** High Power Features, Port 1 ***/
#define hpmd1    ((uint8_t)0x46)
#define cut1     ((uint8_t)0x47)
#define lim1     ((uint8_t)0x48)
#define hpstat1  ((uint8_t)0x49)

/*** High Power Features, Port 2 ***/
#define hpmd2    ((uint8_t)0x4B)
#define cut2     ((uint8_t)0x4C)
#define lim2     ((uint8_t)0x4D)
#define hpstat2  ((uint8_t)0x4E)

/*** High Power Features, Port 3 ***/
#define hpmd3    ((uint8_t)0x50)
#define cut3     ((uint8_t)0x51)
#define lim3     ((uint8_t)0x52)
#define hpstat3  ((uint8_t)0x53)

/*** High Power Features, Port 4 ***/
#define hpmd4    ((uint8_t)0x55)
#define cut4     ((uint8_t)0x56)
#define lim4     ((uint8_t)0x57)
#define hpstat4  ((uint8_t)0x58)



/* initstat; intmask */
#define supply ((uint8_t)0x80)          /* initstat: Supply Interrupt. Set if any of the bits in register 0Ah are set. */
                                        /* intmask: Supply events will pull the INT pin low if this bit is set. */
#define t_start ((uint8_t)0x40)         /* initstat: tSTART Interrupt. Set if any of the tSTART bits in the tsevn register (08h) are set. */
                                        /* intmask: tSTART events will pull the INT pin low if this bit is set. */
#define t_cut ((uint8_t)0x20)           /* initstat: tCUT Interrupt. Set if any of the tCUT bits in the fltevn register (06h) are set or if any of the tLIM bits in the tsevn register (08h) are set. */
                                        /* intmask: tCUT events will pull the INT pin low if this bit is set. */
#define class_initmask ((uint8_t)0x10)  /* initstat: Class Interrupt. Set if any of the class bits in the detevn register (04h) are set. */
                                        /* intmask: Class events will pull the INT pin low if this bit is set. */
#define det ((uint8_t)0x08)             /* initstat: Detect Interrupt. Set if any of the detect bits in the detevn register (04h) are set. */
                                        /* intmask: Detect events will pull the INT pin low if this bit is set. */
#define dis ((uint8_t)0x40)             /* initstat: Disconnect Interrupt. Set if any of the tDIS bits in the fltevn register (06h) are set. */
                                        /* intmask: Disconnect events will pull the INT pin low if this bit is set. */
#define pwrgd ((uint8_t)0x20)           /* initstat: Power Good Interrupt. Set if any of the pwrgd bits in the pwrevn register (02h) are set. */
                                        /* intmask: Power Good events will pull the INT pin low if this bit is set. */
#define pwrena ((uint8_t)0x01)          /* initstat: Power Enable Interrupt. Set if any of the pwrena bits in the pwrevn register (02h) are set. */
                                        /* intmask: Power Enable events will pull the INT pin low if this bit is set. */

/* pwrevn; pwrevn_cor */
#define pwrgd4  (uint8_t)0x80) /* Port 4 Power Good Change. */
#define pwrgd3  (uint8_t)0x40) /* Port 3 Power Good Change. */
#define pwrgd2  (uint8_t)0x20) /* Port 2 Power Good Change. */
#define pwrgd1  (uint8_t)0x10) /* Port 1 Power Good Change. */
#define pwrena4 (uint8_t)0x08) /* Port 4 Power Status Change. */
#define pwrena3 (uint8_t)0x04) /* Port 3 Power Status Change. */
#define pwrena2 (uint8_t)0x02) /* Port 2 Power Status Change. */
#define pwrena1 (uint8_t)0x01) /* Port 1 Power Status Change. */

/* detevn; detevn_cor */
#define class4 ((uint8_t)0x80) /* Port 4 Classification Complete. */
#define class3 ((uint8_t)0x40) /* Port 3 Classification Complete. */
#define class2 ((uint8_t)0x20) /* Port 2 Classification Complete. */
#define class1 ((uint8_t)0x10) /* Port 1 Classification Complete. */
#define det4   ((uint8_t)0x08) /* Port 4 Detection Complete. */
#define det3   ((uint8_t)0x04) /* Port 3 Detection Complete. */
#define det2   ((uint8_t)0x02) /* Port 2 Detection Complete. */
#define det1   ((uint8_t)0x01) /* Port 1 Detection Complete. */

/* fltevn; fltevn _cor */
#define dis4  ((uint8_t)0x80) /* Port 4 disconnect time out (tDIS). */
#define dis3  ((uint8_t)0x40) /* Port 3 disconnect time out (tDIS). */
#define dis2  ((uint8_t)0x20) /* Port 2 disconnect time out (tDIS). */
#define dis1  ((uint8_t)0x10) /* Port 1 disconnect time out (tDIS). */
#define tCUT4 ((uint8_t)0x08) /* Port 4 overcurrent time out (tCUT).  While the port was powered, current exceeded ICUT for longer than tCUT.  */
#define tCUT3 ((uint8_t)0x04) /* Port 3 overcurrent time out (tCUT). */
#define tCUT2 ((uint8_t)0x02) /* Port 2 overcurrent time out (tCUT). */
#define tCUT1 ((uint8_t)0x01) /* Port 1 overcurrent time out (tCUT). */

/* tsevn; tsevn_cor */
#define tLIM4   ((uint8_t)0x80) /* Port 4 current limit time out. The length of the current limit (tLIM) is controlled by the tLIM4 field of tlim34 at 1Fh.   */
#define tLIM3   ((uint8_t)0x40) /* Port 3 current limit time out. The length of the current limit (tLIM) is controlled by the tLIM3 field of tlim34 at 1Fh.   */
#define tLIM2   ((uint8_t)0x20) /* Port 2 current limit time out. The length of the current limit (tLIM) is controlled by the tLIM2 field of tlim12 at 1Eh.   */
#define tLIM1   ((uint8_t)0x10) /* Port 1 current limit time out. The length of the current limit (tLIM) is controlled by the tLIM1 field of tlim12 at 1Eh.   */
#define tSTART4 ((uint8_t)0x08) /* Port 4 startup overcurrent time out (tSTART). While turning on the port, the PD has drawn more than 425mA for longer than tSTART causing the port to turn off. */
#define tSTART3 ((uint8_t)0x04) /* Port 3 startup overcurrent time out (tSTART). */
#define tSTART2 ((uint8_t)0x02) /* Port 2 startup overcurrent time out (tSTART). */
#define tSTART1 ((uint8_t)0x01) /* Port 1 startup overcurrent time out (tSTART). */

/* supevn; supevn_cor */
#define overtemp      ((uint8_t)0x80) /* Over Temperature. Set when die temperature is too high and operation of most functions are disabled.  The tsd bit in the wdog register (42h) will remain set while the condition persists. After the overtemperature condition clears, PD detection will not resume until detect and class are renabled.  */
#define fetbad_supevn ((uint8_t)0x40) /* An external FET has likely failed. */
#define uvlo3         ((uint8_t)0x20) /* Digital Supply (VDD) UVLO. Set when the PSE comes out of Undervoltage Lockout (UVLO). */
#define uvlo48        ((uint8_t)0x10) /* Analog Supply (VEE) Undervoltage Lockout (UVLO). Set when the 48V supply is too low for the PSE to operate properly. Cannot be cleared while the condition persists.  */

/* statp1; statp2; statp3; statp4 */
#define poepp  ((uint8_t)0x80) /* If set, redefines the last classification result for port 1..4 as a PoE++ result. 
                                  0=Reserved; 
                                  1=52.7W; 
                                  2=70W; 
                                  3=90W; 
                                  4=Reserved; 
                                  5=Reserved; 
                                  6=38.7W; 
                                  7=Reserved.*/
#define class_statp  ((uint8_t)0x70) /* Last classification result for port 1..4. 
                                        0=Unknown; 
                                        1=Class 1; 
                                        2=Class 2; 
                                        3=Class 3; 
                                        4=Class 4; 
                                        5=Reserved; 
                                        6=Class 0; 
                                        7=Overcurrent. */
#define detect ((uint8_t)0x07) /* Last detection result for port 1..4. 
                                  0=Unknown; 
                                  1=Short; 
                                  2=CPD Too High; 
                                  3=RSIG Low; 
                                  4=Good; 
                                  5=RSIG High; 
                                  6=Open; 
                                  7=Reserved. */

/* statpwr */
#define pg4 ((uint8_t)0x80) /* Power good on port 4. */
#define pg3 ((uint8_t)0x40) /* Power good on port 3. */
#define pg2 ((uint8_t)0x20) /* Power good on port 2. */
#define pg1 ((uint8_t)0x10) /* Power good on port 1. */
#define pe4 ((uint8_t)0x08) /* Power enabled on port 4. */
#define pe3 ((uint8_t)0x04) /* Power enabled on port 3. */
#define pe2 ((uint8_t)0x02) /* Power enabled on port 2. */
#define pe1 ((uint8_t)0x01) /* Power enabled on port 1. */

/* statpin */
#define ad3  ((uint8_t)0x20) /* SMBus address 3 (AD3 pin logic state). */
#define ad2  ((uint8_t)0x10) /* SMBus address 2 (AD2 pin logic state). */
#define ad1  ((uint8_t)0x08) /* SMBus address 1 (AD1 pin logic state). */
#define ad0  ((uint8_t)0x04) /* SMBus address 0 (AD0 pin logic state). */
#define mid  ((uint8_t)0x02) /* MID pin logic state. */
#define auto ((uint8_t)0x01) /* AUTO pin logic state. */

/* opmd */
#define opmd4_shutdown  ((uint8_t)(0x00 << 6)) /* shutdown port 4 */
#define opmd3_shutdown  ((uint8_t)(0x00 << 4)) /* shutdown port 3 */
#define opmd2_shutdown  ((uint8_t)(0x00 << 2)) /* shutdown port 2 */
#define opmd1_shutdown  ((uint8_t)(0x00 << 0)) /* shutdown port 1 */
#define opmd4_manual    ((uint8_t)(0x01 << 6)) /* port 4 in manual mode */
#define opmd3_manual    ((uint8_t)(0x01 << 4)) /* port 3 in manual mode */
#define opmd2_manual    ((uint8_t)(0x01 << 2)) /* port 2 in manual mode */
#define opmd1_manual    ((uint8_t)(0x01 << 0)) /* port 1 in manual mode */
#define opmd4_semiauto  ((uint8_t)(0x02 << 6)) /* port 4 in semi-auto mode */
#define opmd3_semiauto  ((uint8_t)(0x02 << 4)) /* port 3 in semi-auto mode */
#define opmd2_semiauto  ((uint8_t)(0x02 << 2)) /* port 2 in semi-auto mode */
#define opmd1_semiauto  ((uint8_t)(0x02 << 0)) /* port 1 in semi-auto mode */
#define opmd4_auto      ((uint8_t)(0x03 << 6)) /* port 4 in auto mode */
#define opmd3_auto      ((uint8_t)(0x03 << 4)) /* port 3 in auto mode */
#define opmd2_auto      ((uint8_t)(0x03 << 2)) /* port 2 in auto mode */
#define opmd1_auto      ((uint8_t)(0x03 << 0)) /* port 1 in auto mode */

/* disena */
#define eml_ac4 ((uint8_t)0x80) /* Emulate AC disconnect on port 4. Because AC disconnect is emulated with DC disconnect, setting this bit is equivalent to setting the dc4 bit.  */
#define eml_ac3 ((uint8_t)0x40) /* Emulate AC disconnect on port 3. */
#define eml_ac2 ((uint8_t)0x20) /* Emulate AC disconnect on port 2. */
#define eml_ac1 ((uint8_t)0x10) /* Emulate AC disconnect on port 1. */
#define dc4     ((uint8_t)0x08) /* Enable DC disconnect on port 4. */
#define dc3     ((uint8_t)0x04) /* Enable DC disconnect on port 3. */
#define dc2     ((uint8_t)0x02) /* Enable DC disconnect on port 2. */
#define dc1     ((uint8_t)0x01) /* Enable DC disconnect on port 1. */

/* detena */
#define cls4 ((uint8_t)0x80) /* Enable classification on port 4. In AUTO pin and semi-auto modes setting this bit enables classification following a successful detect. In manual mode setting this bit causes the port to perform one classification then clear the bit. In shutdown mode this bit has no effect.  */
#define cls3 ((uint8_t)0x40) /* Enable classification on port 3. */
#define cls2 ((uint8_t)0x20) /* Enable classification on port 2. */
#define cls1 ((uint8_t)0x10) /* Enable classification on port 1. */
#define det4 ((uint8_t)0x08) /* Enable detection on port 4. In AUTO pin and semi-auto modes setting this bit enables detection. The PSE will periodically detect and report the result. In manual mode when this bit is set the port performs one detection then clears the bit. In shutdown mode this bit has no effect.    */
#define det3 ((uint8_t)0x04) /* Enable detection on port 3. */
#define det2 ((uint8_t)0x02) /* Enable detection on port 2. */
#define det1 ((uint8_t)0x01) /* Enable detection on port 1. */

/* midspan */
#define midsp4 ((uint8_t)0x08) /* Enable midspan backoff on port 4. */
#define midsp3 ((uint8_t)0x04) /* Enable midspan backoff on port 3. */
#define midsp2 ((uint8_t)0x02) /* Enable midspan backoff on port 2. */
#define midsp1 ((uint8_t)0x01) /* Enable midspan backoff on port 1. */

/* mconf */
#define intena  ((uint8_t)0x80) /* Interrupt pin enable. When this bit is cleared the INT pin will be high impedance regardless of the state of registers 00h and 01h. */
#define detchg  ((uint8_t)0x40) /* When this bit is set, detect events are generated only when the result is different from the last detect result on that port. When this bit is cleared, a port�s bit the detevn register is set everytime the PSE performs detection on that port. */
/* Bit 5: To emulate the LTC4259 this bit is set by default. It has no effect. */
#define fast_iv ((uint8_t)0x10) /* When this bit is set, the PSE measures a port�s current and voltage to 9 bits accuracy in shorter measurement time. The 9 bit result is shifted so it�s LSB corresponds with the 14 bit result the PSE reports if this bit is cleared. */
#define msdmsk4 ((uint8_t)0x08) /* MSD pin mask, port 4. When this bit is set pulling the MSD Pin low will reset port 4. */
#define msdmsk3 ((uint8_t)0x04) /* MSD pin mask, port 3. When this bit is set pulling the MSD Pin low will reset port 3. */
#define msdmsk2 ((uint8_t)0x02) /* MSD pin mask, port 2. When this bit is set pulling the MSD Pin low will reset port 2. */
#define msdmsk1 ((uint8_t)0x01) /* MSD pin mask, port 1. When this bit is set pulling the MSD Pin low will reset port 1. */

/* detpb */
#define cls4 ((uint8_t)0x80) /* Set bit 7 in the detena register at 14h, which enables classification on port 4. */
#define cls3 ((uint8_t)0x40) /* Set bit 6 in the detena register at 14h, which enables classification on port 3. */
#define cls2 ((uint8_t)0x20) /* Set bit 5 in the detena register at 14h, which enables classification on port 2. */
#define cls1 ((uint8_t)0x10) /* Set bit 4 in the detena register at 14h, which enables classification on port 1. */
#define det4 ((uint8_t)0x08) /* Set bit 3 in the detena register at 14h, which enables classification on port 4. */
#define det3 ((uint8_t)0x04) /* Set bit 2 in the detena register at 14h, which enables classification on port 3. */
#define det2 ((uint8_t)0x02) /* Set bit 1 in the detena register at 14h, which enables classification on port 2. */
#define det1 ((uint8_t)0x01) /* Set bit 0 in the detena register at 14h, which enables classification on port 1. */

/* pwrpb */
#define off4 ((uint8_t)0x80) /* Turn off port 4. Setting this bit also clears the related detect and fault event bits, the port status register, and the detection and classification enable bits. */
#define off3 ((uint8_t)0x40) /* Turn off port 3. */
#define off2 ((uint8_t)0x20) /* Turn off port 2. */
#define off1 ((uint8_t)0x10) /* Turn off port 1. */
#define on4  ((uint8_t)0x08) /* Turn port 4 power on */
#define on3  ((uint8_t)0x04) /* Turn port 3 power on */
#define on2  ((uint8_t)0x02) /* Turn port 2 power on */
#define on1  ((uint8_t)0x01) /* Turn port 1 power on */

/* rstpb */
#define intclr ((uint8_t)0x80) /* Clear all event registers (02h through 0Bh). */
#define pinclr ((uint8_t)0x40) /* Clear interrupt pin. Setting this bit releases the interrupt pin if it is asserted without affecting the Event registers or the Interrupt register. When the interrupt pin is released in this way, the condition causing the PSE to assert the interrupt pin must be removed before the PSE will be able to assert the interrupt pin again.  */
#define rstall ((uint8_t)0x10) /* Reset the PSE. Setting this bit is identical to pulling the RESET pin low. */
#define rst4   ((uint8_t)0x08) /* Reset port 4. Setting this bit is identical to setting off4 in the pwrpb register. */
#define rst3   ((uint8_t)0x04) /* Reset port 3. */
#define rst2   ((uint8_t)0x02) /* Reset port 2. */
#define rst1   ((uint8_t)0x01) /* Reset port 1. */

/* id */
#define dev ((uint8_t)0xF8) /* Device identification number. */
#define rev ((uint8_t)0x07) /* Revision number. */

/* tlim12 */
#define tlim2 ((uint8_t)0xF0) /* tLIM Timer Duration on port 2. Timer duration is 1.7ms(typ) times the value in this field.If this field is zero, the timer is disabled */
#define tlim1 ((uint8_t)0xF0) /* tLIM Timer Duration on port 1. Timer duration is 1.7ms(typ) times the value in this field.If this field is zero, the timer is disabled */

/* tlim34 */
#define tlim4 ((uint8_t)0xF0) /* tLIM Timer Duration on port 4. Timer duration is 1.7ms(typ) times the value in this field.If this field is zero, the timer is disabled */
#define tlim3 ((uint8_t)0xF0) /* tLIM Timer Duration on port 3. Timer duration is 1.7ms(typ) times the value in this field.If this field is zero, the timer is disabled */

/* wdog */
#define tsd    ((uint8_t)0x80) /* TSD monitor. This bit is set when the chip is in thermal shutdown. */
#define wddis  ((uint8_t)0x1E) /* Watchdog disable. When this field is set to 1011b the watchdog is disabled. Otherwise the SMBus watchdog is enabled. */
#define wdstat ((uint8_t)0x01) /* Status of the watchdog. When the watchdog times out, this bit is set and all ports are reset. This bit must be clear for ports to be enabled. */

/* devid */
#define features ((uint8_t)0xF8) /* Device features, Read Only 
                                    10000b = Gen 3, C-grade 802.3at Type 1;  
                                    01000b = Gen 3 A-grade PoE++ 38.7W;  01010b = Gen 3 A-grade PoE++ 52.7W;  
                                    01100b = Gen 3 A-grade PoE++ 70W;  
                                    01110b = Gen 3 A-grade PoE++ 90W.  
                                    All other encodings are reserved in the LTC4266A/LTC4266C. */
#define numports ((uint8_t)0x07) /* Number of Ports, Read Only. 
                                    000b = 4-port. 
                                    All other encodings reserved in the LTC4266A/LTC4266C. */

/* hpen */
#define poeppen4 ((uint8_t)0x80) /* Enable PoE++ features on port 4. When this bit is set, LTPoE++ physical classification is enabled and extended physical classification information will be posted to the statp4 register. */
#define poeppen3 ((uint8_t)0x80) /* Enable PoE++ features on port 3. When this bit is set, LTPoE++ physical classification is enabled and extended physical classification information will be posted to the statp3 register. */
#define poeppen2 ((uint8_t)0x80) /* Enable PoE++ features on port 2. When this bit is set, LTPoE++ physical classification is enabled and extended physical classification information will be posted to the statp2 register. */
#define poeppen1 ((uint8_t)0x80) /* Enable PoE++ features on port 1. When this bit is set, LTPoE++ physical classification is enabled and extended physical classification information will be posted to the statp1 register. */
#define hpen4    ((uint8_t)0x08) /* Enable high power features on port 4. When this bit is cleared, high power features are disabled and registers 55h through 58h have no effect. */
#define hpen3    ((uint8_t)0x04) /* Enable high power features on port 3. When this bit is cleared, high power features are disabled and registers 50h through 53h have no effect. */
#define hpen2    ((uint8_t)0x02) /* Enable high power features on port 2. When this bit is cleared, high power features are disabled and registers 4Bh through 4Eh have no effect. */
#define hpen1    ((uint8_t)0x01) /* Enable high power features on port 1. When this bit is cleared, high power features are disabled and registers 46h through 49h have no effect. */

/* hpmd1; hpmd2; hpmd3; hpmd4 */
#define legen ((uint8_t)0x02) /* Enable detection of legacy PDs by sensing for large capacitance on port 1..4. When this bit is set a PD with a large common mode capacitance will be reported as a valid signature (code 4 in the statp1 register). */
#define pongen ((uint8_t)0x01) /* Enable Ping-Pong classification on port 1..4. Not functional on the LTC4266C. */

/* cut1; cut2; cut3; cut4 */
#define rdis   ((uint8_t)0x80) /* Set to match the port 1..4 sense resistor value: 0=0.5?, 1=0.25?. */
#define cutrng ((uint8_t)0x40) /* Adjust overcurrent threshold range on port 1..4. See Table 6. */
#define cut    ((uint8_t)0x3F) /* Set threshold of overcurrent comparator on port 1..4. See Table 6. */

/* hpstat1; hpstat2; hpstat3; hpstat4 */
#define fetbad_hpstat ((uint8_t)0x02) /* The External FET has likely failed. See the MOSFET Fault Detection section for more information. */
#define pongpd        ((uint8_t)0x01) /* This bit is set when a Ping-Pong classification has occurred. */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void ltc4266_set_port_pins(API_GPIO_type_t* _nint,
  API_GPIO_type_t* _auto,
  API_GPIO_type_t* _nmsd,
  API_GPIO_type_t* _nreset,
  API_GPIO_type_t* _mid,
  API_GPIO_type_t* _nshdn1,
  API_GPIO_type_t* _nshdn2,
  API_GPIO_type_t* _nshdn3,
  API_GPIO_type_t* _nshdn4,
  API_GPIO_type_t* _sda,
  API_GPIO_type_t* _scl);
extern void ltc4266_get_config(uint8_t address);
extern void ltc4266_set_config(uint8_t address, ltc4266_config_t* ltc4266_config);
extern void ltc4266_init(uint8_t address);

#endif //__LTC4266_H
