var searchData=
[
  ['lptim1_5firqhandler',['LPTIM1_IRQHandler',['../LPTIMER_8c.html#af395599d8a77faa78d99c1beb0e72de3',1,'LPTIMER.c']]],
  ['lptimer_5finit',['LPTIMER_Init',['../LPTIMER_8c.html#a2d4f5708d50ff6ccc3c313ed3321800d',1,'LPTIMER_Init(void(*handler)(double t_period_s), double t_period_s):&#160;LPTIMER.c'],['../LPTIMER_8h.html#a2d4f5708d50ff6ccc3c313ed3321800d',1,'LPTIMER_Init(void(*handler)(double t_period_s), double t_period_s):&#160;LPTIMER.c']]],
  ['lptimer_5fstart',['LPTIMER_Start',['../LPTIMER_8c.html#a29af84c81e9838ea466fd0aa2e180748',1,'LPTIMER_Start(void):&#160;LPTIMER.c'],['../LPTIMER_8h.html#a29af84c81e9838ea466fd0aa2e180748',1,'LPTIMER_Start(void):&#160;LPTIMER.c']]],
  ['lpuart_5fdma_5finit',['LPUART_DMA_Init',['../LPUART_8c.html#aeff5ab457831144b784114f6da09a5de',1,'LPUART.c']]],
  ['lpuart_5finit',['LPUART_Init',['../LPUART_8c.html#a01a78e16c0677cbfc83bab0e604f886b',1,'LPUART_Init(GPIOInstance_t *lpuart_port_pin_rx, GPIOAlternativeFunction_t lpuart_alternative_function_rx, GPIOInstance_t *lpuart_port_pin_tx, GPIOAlternativeFunction_t lpuart_alternative_function_tx, uint32_t baudrate, void(*handler)(uint8_t *buffer)):&#160;LPUART.c'],['../LPUART_8h.html#a01a78e16c0677cbfc83bab0e604f886b',1,'LPUART_Init(GPIOInstance_t *lpuart_port_pin_rx, GPIOAlternativeFunction_t lpuart_alternative_function_rx, GPIOInstance_t *lpuart_port_pin_tx, GPIOAlternativeFunction_t lpuart_alternative_function_tx, uint32_t baudrate, void(*handler)(uint8_t *buffer)):&#160;LPUART.c']]],
  ['lpuart_5freceived_5fhandler',['lpuart_received_handler',['../application_8c.html#a9cbc3d9fe770f18edadfac923e609164',1,'application.c']]],
  ['lpuart_5fsend',['LPUART_Send',['../LPUART_8c.html#ad4eaf405476d9a39f4024b41dedf91bb',1,'LPUART_Send(uint8_t *buffer, uint8_t buffer_len, uint8_t send_crc):&#160;LPUART.c'],['../LPUART_8h.html#ad4eaf405476d9a39f4024b41dedf91bb',1,'LPUART_Send(uint8_t *buffer, uint8_t buffer_len, uint8_t send_crc):&#160;LPUART.c']]]
];
