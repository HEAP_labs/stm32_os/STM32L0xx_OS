/**
 * @file    ./SystemCore/inc/mcu.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file to define MCU
 */

#ifndef __MCU_H
#define __MCU_H

/* Exported constants --------------------------------------------------------*/
#ifndef STM32L011xx
    #define STM32L011xx
#endif // !STM32L011xx

#ifndef STM32L011K4
    #define STM32L011K4
#endif // !STM32L011K4

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx.h"

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */


#endif // __MCU_H

