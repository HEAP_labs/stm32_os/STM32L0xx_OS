/**
 * @file    ./API/src/USART.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the USART module
 */

#include "PerUsings.h"
#ifdef __USING_USART

/* Includes ------------------------------------------------------------------*/
#include "USART.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void API_USART_DMA_init(uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t priority);
void API_USART_init(API_GPIO_type_t * usart_port_pin_rx,
        API_GPIO_alternative_function_t usart_alternative_function_rx,
        API_GPIO_type_t * usart_port_pin_tx,
        API_GPIO_alternative_function_t usart_alternative_function_tx,
        uint32_t baudrate);
void API_USART_DMA_send(uint8_t tx_length);
void API_USART_DMA_receive(uint16_t rx_length);

/* Private functions ---------------------------------------------------------*/

/*!
 * @brief Initialize DMA for USART
 */
void API_USART_DMA_init(uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t priority) {
    /* Enable the peripheral clock DMA1 */
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;

    DMA1_Channel4->CCR &= ~DMA_CCR_EN;
    DMA1_Channel5->CCR &= ~DMA_CCR_EN;

    /* DMA1 Channel4 USART2_TX config */
    /* Map USART2_TX DMA channel */
    DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C4S) | (4 << (3 * 4));
    /* Peripheral address */
    DMA1_Channel4->CPAR = (uint32_t) &(USART2->TDR);
    /* Memory address */
    DMA1_Channel4->CMAR = (uint32_t) p_tx_data;
    /* Memory increment */
    /* Memory to peripheral */
    /* 8-bit transfer */
    /* Transfer complete IT */
    DMA1_Channel4->CCR = DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE;

    /* DMA1 Channel5 USART2_RX config */
    /* Map USART2_RX DMA channel */
    DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C5S) | (4 << (4 * 4));
    /* Peripheral address */
    DMA1_Channel5->CPAR = (uint32_t) &(USART2->RDR);
    /* Memory address */
    DMA1_Channel5->CMAR = (uint32_t) p_rx_data;
    /* Memory increment */
    /* Peripheral to memory*/
    /* 8-bit transfer */
    /* Transfer complete IT */
    DMA1_Channel5->CCR = DMA_CCR_MINC | DMA_CCR_TCIE | DMA_CCR_EN;

    /* Configure IT */
    /* Set priority for DMA1_Channel4_5_6_7_IRQn */
    NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, priority);
}

/*!
 * @brief Initialize USART
 * @param usart_port_pin_rx GPIO Instance of the RX-Pin
 * @param usart_alternative_function_rx Alternative-Function Number of the RX-Pin
 * @param usart_port_pin_tx GPIO Instance of the TX-Pin
 * @param usart_alternative_function_tx Alternative-Function Number of the TX-Pin
 * @param baudrate Baudrate used for the Low-Power UART
 */
void API_USART_init(API_GPIO_type_t * usart_port_pin_rx,
        API_GPIO_alternative_function_t usart_alternative_function_rx,
        API_GPIO_type_t * usart_port_pin_tx,
        API_GPIO_alternative_function_t usart_alternative_function_tx,
        uint32_t baudrate) {

    /* Initialize GPIO */
    usart_port_pin_rx->GPIOX_Mode = ALTERNATIVE_FUNCTION;
    usart_port_pin_rx->GPIOX_Type = PUSH_PULL;
    usart_port_pin_rx->GPIOX_AlternativeFunction =
            usart_alternative_function_rx;
    usart_port_pin_tx->GPIOX_Mode = ALTERNATIVE_FUNCTION;
    usart_port_pin_tx->GPIOX_Type = PUSH_PULL;
    usart_port_pin_tx->GPIOX_AlternativeFunction =
            usart_alternative_function_tx;
    API_GPIO_init(usart_port_pin_rx);
    API_GPIO_init(usart_port_pin_tx);        

   /* Enable the peripheral clock USART2 */
    RCC->APB1ENR |= RCC_APB1ENR_USART2EN;

    /* Configure USART2 */
    /* oversampling by 16, set baudrate */
    USART2->BRR = SystemCoreClock / (baudrate);
    /* 8 data bit, 1 start bit, 1 stop bit, no parity, reception and transmission enabled */
    USART2->CR1 = USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;
}

/*!
 * @brief Send function for the USART using DMA
 * @param tx_length length of the buffer in bytes
 */
void API_USART_DMA_send(uint8_t tx_length) {
    /* polling idle frame Transmission */
    while (DMA1_Channel4->CNDTR != 0) {
    }

    /* start 8-bit transmission with DMA */
    /* disable DMA Channel 4 */
    DMA1_Channel4->CCR &= ~ DMA_CCR_EN;
    /* size of the buffer */
    DMA1_Channel4->CNDTR = tx_length;
    /* Enable DMA transmission */
    USART2->CR3 |= USART_CR3_DMAT;
    /* Enable DMA1_Channel4_5_6_7_IRQn */
    NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
    /* enable DMA Channel 4 */
    DMA1_Channel4->CCR |= DMA_CCR_EN;
}

/*!
* @brief Receive USART data using DMA
* @param rx_length length of the buffer in bytes
*/
void API_USART_DMA_receive(uint16_t rx_length)
{
  /* polling idle frame Transmission */
  while (DMA1_Channel5->CNDTR != 0) {
  }

  /* start 8-bit transmission with DMA */
  /* disable DMA Channel 5 */
  DMA1_Channel5->CCR &= ~DMA_CCR_EN;
  /* size of the buffer */
  DMA1_Channel5->CNDTR = 48;
  /* Enable DMA reception */
  USART2->CR3 |= USART_CR3_DMAR;
  /* Enable DMA1_Channel4_5_6_7_IRQn */
  NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
  /* enable DMA Channel 5 */
  DMA1_Channel5->CCR |= DMA_CCR_EN;
}

#endif // __USING_USART
