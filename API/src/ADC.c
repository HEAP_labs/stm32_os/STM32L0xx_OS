/**
 * @file    ./API/src/ADC.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the ADC module
 */

#include "PerUsings.h"
#ifdef __USING_ADC

/* Includes ------------------------------------------------------------------*/
#include "ADC.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/*! @brief supply voltage of the mcu in mV */
double g_adc_ref_mv = 1993;

/* Private function prototypes -----------------------------------------------*/
void API_ADC_init(void);
uint32_t API_ADC_get_voltage(API_GPIO_type_t* adc_pin);
void API_ADC_calibrate(void);
void API_ADC_enable(void);
void API_ADC_disable(void);
/* Private functions ---------------------------------------------------------*/

/*!
 * @brief Starts the ADC conversion and returns the measured ADC voltage
 * @param adc_pin pointer to the API_GPIO_type_t object
 * @return measured ADC voltage in mV
 */
uint32_t API_ADC_get_voltage(API_GPIO_type_t * adc_pin) {
    /* Initialize the GPIO pin */
    API_GPIO_init(adc_pin);
    /* select the ADC channel */
    ADC1->CHSELR = 1 << adc_pin->GPIOX_ADC_Channel;
    /* start the ADC conversion */
    ADC1->CR |= ADC_CR_ADSTART;
    /* wait end of conversion */
    while ((ADC1->ISR & ADC_ISR_EOC) == 0) {
    }
    /* calculate measured voltage and return */
    return (uint32_t) (ADC1->DR * g_adc_ref_mv / 0xFFF0);
}

/*!
 * @brief Calibrates the ADC
 */
void API_ADC_calibrate(void) {
    /* Ensure that ADEN = 0 */
    if ((ADC1->CR & ADC_CR_ADEN) != 0) {
        /* Clear ADEN */
        ADC1->CR &= (uint32_t) (~ADC_CR_ADEN);
    }
    /* Set ADCAL=1 */
    ADC1->CR |= ADC_CR_ADCAL;
    /* Wait until EOCAL=1 */
    while ((ADC1->ISR & ADC_ISR_EOCAL) == 0) {
    }
    /* Clear EOCAL */
    ADC1->ISR |= ADC_ISR_EOCAL;
}

/*!
 * @brief Enables the ADC
 */
void API_ADC_enable(void) {
    /* Enable the ADC */
    ADC1->CR |= ADC_CR_ADEN;
    if ((ADC1->CFGR1 & ADC_CFGR1_AUTOFF) == 0) {
        /* Wait until ADC ready if AUTOFF is not set */
        while ((ADC1->ISR & ADC_ISR_ADRDY) == 0) {
        }
    }
}

/*!
 * @brief Disables the ADC
 */
void API_ADC_disable(void) {
    /* Ensure that no conversion on going */
    if ((ADC1->CR & ADC_CR_ADSTART) != 0) {
        /* Stop any ongoing conversion */
        ADC1->CR |= ADC_CR_ADSTP;
    }
    /* Wait until ADSTP is reset by hardware i.e. conversion is stopped */
    while ((ADC1->CR & ADC_CR_ADSTP) != 0) {
    }
    /* Disable the ADC */
    ADC1->CR |= ADC_CR_ADDIS;
    /* Wait until the ADC is fully disabled */
    while ((ADC1->CR & ADC_CR_ADEN) != 0) {
    }
}

/*!
 * @brief Initializes the ADC
 * @param handler function pointer to the end of conversion function handler
 */
void API_ADC_init() {
    /* Enable the peripheral clock of the ADC and SYSCFG */
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN | RCC_APB2ENR_SYSCFGEN;
    /* Select HSI16 by writing 00 in CKMODE (reset value), shift 2 bits on oversampling, 64 times oversampling, oversampler enable */
    ADC1->CFGR2 = (ADC1->CFGR2
            & ~(ADC_CFGR2_CKMODE | ADC_CFGR2_OVSS | ADC_CFGR2_OVSR))
            | (ADC_CFGR2_OVSS_1 | ADC_CFGR2_OVSR_0 | ADC_CFGR2_OVSR_2
                    | ADC_CFGR2_OVSE);
    /* Select the auto off mode */
    ADC1->CFGR1 |= ADC_CFGR1_AUTOFF | ADC_CFGR1_DISCEN;
    /* Select CHSEL6 */
    ADC1->CHSELR = ADC_CHSELR_CHSEL6;
    /* Select a sampling mode of 111 160.5 ADC clk */
    ADC1->SMPR |= ADC_SMPR_SMPR;
    API_ADC_calibrate();
    API_ADC_enable();
}

#endif // __USING_ADC
