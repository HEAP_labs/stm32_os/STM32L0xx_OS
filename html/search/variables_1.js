var searchData=
[
  ['g_5fb_5fis_5flast_5fboard',['g_b_is_last_board',['../application_8c.html#abd5a655d2ae02e2266a2215f3b2e64d1',1,'application.c']]],
  ['g_5fb_5fmeasurement_5frunning',['g_b_measurement_running',['../application_8c.html#a817070dd4b0bdeb10561d20e8da689cb',1,'application.c']]],
  ['g_5fb_5fstart_5fmeasurement',['g_b_start_measurement',['../application_8c.html#a7882ae0be5c0e7308062c842d3fe7c88',1,'application.c']]],
  ['g_5fboard_5findex',['g_board_index',['../application_8c.html#ad3cadd4aa11ddf117d1ede07739be9b4',1,'application.c']]],
  ['g_5fmeasured_5fdata',['g_measured_data',['../application_8c.html#a27a5c8ab16e81cd991cb5ca2380eedf8',1,'application.c']]],
  ['g_5ft_5fperiod_5fs',['g_t_period_s',['../LPTIMER_8c.html#a1c4f179457199bc33233070d6b1dd934',1,'LPTIMER.c']]],
  ['gpiox_5falternativefunction',['GPIOX_AlternativeFunction',['../structGPIOInstance__s.html#a0191b4f5200120529db7680bddc766d3',1,'GPIOInstance_s']]],
  ['gpiox_5fmode',['GPIOX_Mode',['../structGPIOInstance__s.html#a522ceef5fc75e7b4556d7bfaf2dc1af2',1,'GPIOInstance_s']]],
  ['gpiox_5fpin',['GPIOX_Pin',['../structGPIOInstance__s.html#afe6007e487512e7ca1a066f722207948',1,'GPIOInstance_s']]],
  ['gpiox_5fpull',['GPIOX_Pull',['../structGPIOInstance__s.html#a944734d11ef162cad82d2bbc17cdce68',1,'GPIOInstance_s']]],
  ['gpiox_5fspeed',['GPIOX_Speed',['../structGPIOInstance__s.html#a7932939d4c527d961b4f9dd75ef164a9',1,'GPIOInstance_s']]],
  ['gpiox_5ftype',['GPIOX_Type',['../structGPIOInstance__s.html#a33fb015cc136e93640423fe724d14177',1,'GPIOInstance_s']]]
];
