/**
 * @file    ./API/src/I2C.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the I2C module
 */

#include "PerUsings.h"
//#ifdef __USING_I2C

/* Includes ------------------------------------------------------------------*/
#include "I2C.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t i2c_receive_buffer[25];

/* Private function prototypes -----------------------------------------------*/
static void API_I2C_GPIO_init(API_GPIO_type_t* i2c_port_pin_sda,
                              API_GPIO_alternative_function_t i2c_alternative_function_sda,
                              API_GPIO_type_t* i2c_port_pin_scl,
                              API_GPIO_alternative_function_t i2c_alternative_function_scl);
void API_I2C_init(API_GPIO_type_t* i2c_port_pin_sda,
                  API_GPIO_alternative_function_t i2c_alternative_function_sda,
                  API_GPIO_type_t* i2c_port_pin_scl,
                  API_GPIO_alternative_function_t i2c_alternative_function_scl);
void API_I2C_DMA_init(void);
void API_I2C_DMA_set_reg(uint8_t reg, uint8_t reg_val, uint8_t slave_address);
void API_I2C_DMA_set_regs(uint8_t* reg, uint8_t* reg_val, uint8_t slave_address, uint8_t reg_count);
void API_I2C_DMA_get_reg(uint8_t reg, uint8_t slave_address);
void API_I2C_DMA_get_regs(uint8_t* reg, uint8_t slave_address, uint8_t reg_count);

/* Private functions ---------------------------------------------------------*/
void API_I2C_init(API_GPIO_type_t* i2c_port_pin_sda,
                  API_GPIO_alternative_function_t i2c_alternative_function_sda,
                  API_GPIO_type_t* i2c_port_pin_scl,
                  API_GPIO_alternative_function_t i2c_alternative_function_scl)
{
  /* Enable the peripheral clock I2C1 */
  RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;

  /* Initialize GPIO */
  API_I2C_GPIO_init(i2c_port_pin_sda, i2c_alternative_function_sda, i2c_port_pin_scl, i2c_alternative_function_scl);

  I2C1->TIMINGR = (uint32_t)0x00503D5A;
  I2C1->CR2 |= I2C_CR2_AUTOEND;
  I2C1->CR1 |= I2C_CR1_PE;
}

static void API_I2C_GPIO_init(API_GPIO_type_t* i2c_port_pin_sda,
                              API_GPIO_alternative_function_t i2c_alternative_function_sda,
                              API_GPIO_type_t* i2c_port_pin_scl,
                              API_GPIO_alternative_function_t i2c_alternative_function_scl)
{
  /* Initialize GPIO */
  i2c_port_pin_sda->GPIOX_Mode = ALTERNATIVE_FUNCTION;
  i2c_port_pin_sda->GPIOX_AlternativeFunction = i2c_alternative_function_sda;
  i2c_port_pin_scl->GPIOX_Mode = ALTERNATIVE_FUNCTION;
  i2c_port_pin_scl->GPIOX_AlternativeFunction = i2c_alternative_function_scl;
  API_GPIO_init(i2c_port_pin_sda);
  API_GPIO_init(i2c_port_pin_scl);
}

void API_I2C_DMA_init()
{
  /* Enable the peripheral clock DMA1 */
  RCC->AHBENR |= RCC_AHBENR_DMA1EN;

  I2C1->CR1 |= I2C_CR1_TXDMAEN | I2C_CR1_RXDMAEN;

  /* DMA1 Channel4 I2C1_TX config */
  /* (1)  Map I2C1_TX DMA channel */
  /* (2)  Peripheral address */
  /* (3)  Memory address */
  /* (4)  Memory increment */
  /*      Memory to peripheral */
  /*      8-bit transfer */
  DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C4S) | (6 << (3 * 4)); /* (1) */
  DMA1_Channel4->CPAR = (uint32_t) & (I2C1->TXDR); /* (2) */
  DMA1_Channel4->CCR |= DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE; /* (4) */

  DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C3S) | (6 << (2 * 4));
  DMA1_Channel3->CPAR = (uint32_t) & (I2C1->RXDR);
  DMA1_Channel3->CCR |= DMA_CCR_MINC | DMA_CCR_TCIE;

  /* Configure IT */
  NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 1);
  NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
}


void API_I2C_DMA_set_reg(uint8_t reg, uint8_t reg_val, uint8_t slave_address)
{
  static uint8_t buffer[2];

  buffer[0] = reg;
  buffer[1] = reg_val;

  if((I2C1->ISR & I2C_ISR_TXE) == (I2C_ISR_TXE)) /* Check Tx empty */
  {
    I2C1->CR2 |= (2 << 16) | (slave_address << 1);
    I2C1->CR2 &= ~I2C_CR2_RD_WRN;
    DMA1_Channel4->CCR &= ~DMA_CCR_EN;
    DMA1_Channel4->CMAR = (uint32_t)buffer;
    DMA1_Channel4->CNDTR = 2; // Data size 
    DMA1_Channel4->CCR |= DMA_CCR_EN;

    I2C1->CR2 |= I2C_CR2_START; /* Go */
  }
}

//reg_count max: 20
void API_I2C_DMA_set_regs(uint8_t* reg, uint8_t* reg_val, uint8_t slave_address, uint8_t reg_count)
{
  uint8_t i;
  uint8_t reg_count_limited;
  static uint8_t buffer[50];

  if(reg_count <= 25) { reg_count_limited = reg_count; }
  else { reg_count_limited = 25; }

  for(i = 0; i < reg_count_limited; i++)
  {
    buffer[2 * i] = reg[i];
    buffer[2 * i + 1] = reg_val[i];
  }

  if((I2C1->ISR & I2C_ISR_TXE) == (I2C_ISR_TXE)) /* Check Tx empty */
  {
    I2C1->CR2 |= (reg_count_limited << 16) | (slave_address << 1);
    I2C1->CR2 &= ~I2C_CR2_RD_WRN;
    DMA1_Channel4->CCR &= ~DMA_CCR_EN;
    DMA1_Channel4->CMAR = (uint32_t)buffer;
    DMA1_Channel4->CNDTR = reg_count_limited; /* Data size */
    DMA1_Channel4->CCR |= DMA_CCR_EN;

    I2C1->CR2 |= I2C_CR2_START; /* Go */
  }
}

void API_I2C_DMA_get_reg(uint8_t reg, uint8_t slave_address)
{
  static uint8_t buffer[2];

  buffer[0] = reg;
  buffer[1] = 0;

  if((I2C1->ISR & I2C_ISR_TXE) == (I2C_ISR_TXE)) /* Check Tx empty */
  {
    I2C1->CR2 |= I2C_CR2_RD_WRN | (2 << 16) | (slave_address << 1);

    DMA1_Channel4->CCR &= ~DMA_CCR_EN;
    DMA1_Channel4->CMAR = (uint32_t)buffer;
    DMA1_Channel4->CNDTR = 2; /* Data size */
    DMA1_Channel4->CCR |= DMA_CCR_EN;

    DMA1_Channel5->CCR &= ~DMA_CCR_EN;
    DMA1_Channel5->CMAR = (uint32_t)i2c_receive_buffer;
    DMA1_Channel5->CNDTR = 2; /* Data size */
    DMA1_Channel5->CCR |= DMA_CCR_EN;

    I2C1->CR2 |= I2C_CR2_START; /* Go */
  }
}

//reg_count max: 25
void API_I2C_DMA_get_regs(uint8_t* reg, uint8_t slave_address, uint8_t reg_count)
{
  uint8_t i;
  uint8_t reg_count_limited;
  static uint8_t buffer[25];

  if(reg_count <= 25) { reg_count_limited = reg_count; }
  else { reg_count_limited = 25; }

  for(i = 0; i < reg_count_limited; i++)
  {
    buffer[i] = reg[i];
  }

  if((I2C1->ISR & I2C_ISR_TXE) == (I2C_ISR_TXE)) /* Check Tx empty */
  {
    I2C1->CR2 |= I2C_CR2_RD_WRN | (reg_count_limited << 16) | (slave_address << 1);

    DMA1_Channel4->CCR &= ~DMA_CCR_EN;
    DMA1_Channel4->CMAR = (uint32_t)buffer;
    DMA1_Channel4->CNDTR = reg_count_limited; /* Data size */
    DMA1_Channel4->CCR |= DMA_CCR_EN;

    DMA1_Channel5->CCR &= ~DMA_CCR_EN;
    DMA1_Channel5->CMAR = (uint32_t)i2c_receive_buffer;
    DMA1_Channel5->CNDTR = reg_count_limited; /* Data size */
    DMA1_Channel5->CCR |= DMA_CCR_EN;

    I2C1->CR2 |= I2C_CR2_START; /* Go */

    I2C1->CR2 |= I2C_CR2_RD_WRN | (reg_count_limited << 16) | (slave_address << 1);
    DMA1_Channel5->CCR &= ~DMA_CCR_EN;
    DMA1_Channel5->CMAR = (uint32_t)& buffer;
    DMA1_Channel5->CNDTR = reg_count_limited; /* Data size */
    DMA1_Channel5->CCR |= DMA_CCR_EN;

    I2C1->CR2 |= I2C_CR2_START; /* Go */
  }
}

//#endif // __USING_I2C
