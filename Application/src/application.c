/**
 * @file    ./src/application.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the application code
 *
 * This file contains the implementation of the application code for the
 * Fittrack Weight Detection Module. The prefixes of the variable names are
 * from the Barr Group Embedded C-Coding Standard and a few more are added.
 * Prefix
 * i....input
 * o....output
 *
 */


/* Includes ------------------------------------------------------------------*/
#include "application.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void taskclass(double t_period_s);
void startup_sequence(void);
static void gpio_config(void);
static void system_clock_config(void);
void error_handler(void);

/* Private functions ---------------------------------------------------------*/
void taskclass(double t_period_s)
{
  static double t = 0;
    
  /* start the low power timer for the next cycle */
  API_LPTIMER_start();
  
  if (t > 0.5) API_setDO(&PB3, 0);
  else         API_setDO(&PB3, 1);
  
  if (t < 1.0) t = t + T_PERIOD_S;
  else         t = 0;
  
  /* Watchdog Heartbeat */
  API_WWDG_heartbeat();
}

/*
* @brief startup - function, called in the main module
* @param[in] NONE
* @return NONE
*/
void startup_sequence(void)
{
  system_clock_config();
  SystemCoreClockUpdate();

  /* configure GPIO */
  gpio_config();
  
#ifdef __USING_LPUART
  API_LPUART_init(LPUART_PORT_PIN_RX,
    LPUART_ALTERNATIVE_FUNCTION_RX,
    LPUART_PORT_PIN_TX,
    LPUART_ALTERNATIVE_FUNCTION_TX,
    LPUART_BAUDRATE);
#endif // __USING_LPUART

#ifdef __USING_USART  
  /* initialize USART */
  API_USART_init(USART_PORT_PIN_RX,
    USART_ALTERNATIVE_FUNCTION_RX,
    USART_PORT_PIN_TX,
    USART_ALTERNATIVE_FUNCTION_TX,
    USART_BAUDRATE); 
#endif // __USINIG_USART
  
#ifdef __USING_WWDG
  /* Initialize Watchdog */
  API_WWDG_init(&error_handler);
#endif // __USING_WWDG
  
#ifdef __USING_LPTIMER
  /* initialize Low Power Timer */
  API_LPTIMER_init(&taskclass, T_PERIOD_S);
  
  API_LPTIMER_start();
#endif // __USING_LPTIMER
  
#ifndef DEBUG
  /* Enable sleep on exit */
  SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;

  __WFI();
#endif // !DEBUG
}

/*!
* @brief configures the GPIO
*/
static void gpio_config(void)
{
    PB3.GPIOX_Type = PUSH_PULL;
    PB3.GPIOX_Mode = OUTPUT;
    API_GPIO_init(&PB3);
}

/*!
* @brief config clock sources and prescalers
* @note  SYSCLK=32MHz
*/
static void system_clock_config(void)
{
  /* Enable power interface clock */
  RCC->APB1ENR |= (RCC_APB1ENR_PWREN);

  /* Select voltage scale 1 (1.65V - 1.95V)
  i.e. (01)  for VOS bits in PWR_CR */
  PWR->CR = (PWR->CR & ~(PWR_CR_VOS)) | PWR_CR_VOS_0;

  /* Enable HSI RCC-> CR */
  RCC->CR |= RCC_CR_HSION;

  /* Wait for HSI ready flag */
  while ((RCC->CR & RCC_CR_HSIRDY) == 0);

  /* Set PLL on HSI, multiply by 4 and divided by 2 */
  RCC->CFGR |= RCC_CFGR_PLLSRC_HSI | RCC_CFGR_PLLMUL4 | RCC_CFGR_PLLDIV2;

  /* Enable the PLL in RCC_CR register */
  RCC->CR |= RCC_CR_PLLON;

  /* Wait for PLL ready flag */
  while ((RCC->CR & RCC_CR_PLLRDY) == 0); 

  /* Select PLL as system clock */
  RCC->CFGR |= RCC_CFGR_SW_PLL;

  /* Wait for clock switched on PLL */
  while ((RCC->CFGR & RCC_CFGR_SWS_PLL) == 0);
}

/*!
* @brief error handler, called before the reset from the watchdog
*/
void error_handler(void)
{
    
}


