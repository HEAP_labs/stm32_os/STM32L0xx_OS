/**
 * @file    ./Library/inc/Library.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file to include Libraries defiened in LibUsings.h
 */

#ifndef __LIBRARY_H
#define __LIBRARY_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "LibUsings.h"

#ifdef __USING_CRC16
    #include "CRC16.h"
#endif // __USING_CRC16

#ifdef __USING_LTC4266
    #include "LTC4266.h"
#endif // __USING_LTC4266

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __LIBRARY_H
