/**
 * @file    ./API/inc/WWDG.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the WWDG.c module
 */

#ifndef __WWDG_H
#define __WWDG_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void API_WWDG_init(void (*handler)(void));
void API_WWDG_heartbeat(void);
void API_WWDG_enable(void);
void API_WWDG_disable(void);

#endif /* _WWDG_H */

