/**
 * @file    ./Library/src/LTC4266.c
 * @author  Andreas Hirtenlehner
 * @brief   Library for PoE IC (LTC4266)
 */

#include "LibUsings.h"
#ifdef __USING_LTC4266

#include "LTC4266.h"

API_GPIO_type_t* port_pin_nint;
API_GPIO_type_t* port_pin_auto;
API_GPIO_type_t* port_pin_nmsd;
API_GPIO_type_t* port_pin_nreset;
API_GPIO_type_t* port_pin_mid;
API_GPIO_type_t* port_pin_nshdn1;
API_GPIO_type_t* port_pin_nshdn2;
API_GPIO_type_t* port_pin_nshdn3;
API_GPIO_type_t* port_pin_nshdn4;
API_GPIO_type_t* port_pin_sda;
API_GPIO_type_t* port_pin_scl;

uint16_t i_lim_mA[] = { 106, 213, 319, 425, 531, 638, 744, 850, 956, 1063, 1169, 1275, 1488, 1700, 1913, 2125, 2338, 2550, 2975 };
uint8_t r_sense[] = { 0x88, 0x08, 0x89, 0x80, 0x8A, 0x90, 0x9A, 0xC0, 0xCA, 0xD0, 0xDA, 0xE0, 0x49, 0x40, 0x4A, 0x50, 0x5A, 0x60, 0x52 };


void ltc4266_set_port_pins(API_GPIO_type_t* _nint,
  API_GPIO_type_t* _auto,
  API_GPIO_type_t* _nmsd,
  API_GPIO_type_t* _nreset,
  API_GPIO_type_t* _mid,
  API_GPIO_type_t* _nshdn1,
  API_GPIO_type_t* _nshdn2,
  API_GPIO_type_t* _nshdn3,
  API_GPIO_type_t* _nshdn4,
  API_GPIO_type_t* _sda,
  API_GPIO_type_t* _scl);
void ltc4266_get_config(uint8_t address);
void ltc4266_set_config(uint8_t address, ltc4266_config_t* ltc4266_config);
void ltc4266_init(uint8_t address);

void ltc4266_set_port_pins(API_GPIO_type_t* _nint,
  API_GPIO_type_t* _auto,
  API_GPIO_type_t* _nmsd,
  API_GPIO_type_t* _nreset,
  API_GPIO_type_t* _mid,
  API_GPIO_type_t* _nshdn1,
  API_GPIO_type_t* _nshdn2,
  API_GPIO_type_t* _nshdn3,
  API_GPIO_type_t* _nshdn4,
  API_GPIO_type_t* _sda,
  API_GPIO_type_t* _scl)
{
  port_pin_nint = _nint;
  port_pin_auto = _auto;
  port_pin_nmsd = _nmsd;
  port_pin_nreset = _nreset;
  port_pin_mid = _mid;
  port_pin_nshdn1 = _nshdn1;
  port_pin_nshdn2 = _nshdn2;
  port_pin_nshdn3 = _nshdn3;
  port_pin_nshdn4 = _nshdn4;
  port_pin_sda = _sda;
  port_pin_scl = _scl;
}

void ltc4266_init(uint8_t address)
{
  uint8_t reg[] = { intmask,
                    opmd,
                    disena,
                    detena,
                    midspan,
                    mconf,
                    wdog,
                    hpen,
                    hpmd1,
                    cut1,
                    lim1,
                    hpmd2,
                    cut2,
                    lim2, 
                    hpmd3,
                    cut3,
                    lim3, 
                    hpmd4,
                    cut4,
                    lim4
                  };

  uint8_t reg_val[] = { supply | t_start | t_cut | class_initmask | det | dis | pwrgd | pwrena,
                        opmd1_shutdown | opmd2_shutdown | opmd3_shutdown | opmd4_shutdown,
                        /* ??? */0,
                        0, // disable classification and detection
                        midsp1 | midsp2 | midsp3 | midsp4,
                        intena,
                        wddis,
                        hpen1 | hpen2 | hpen3 | hpen4 | poeppen1 | poeppen2 | poeppen3 | poeppen4,
                        pongen,      // Enable Ping-Pong classification 
                        rdis | 0x11, // Type 2 Icut
                        0xC0,        // Type 2 Ilim
                        pongen,      // Enable Ping-Pong classification 
                        rdis | 0x11, // Type 2 Icut
                        0xC0,        // Type 2 Ilim
                        pongen,      // Enable Ping-Pong classification 
                        rdis | 0x11, // Type 2 Icut
                        0xC0,        // Type 2 Ilim
                        pongen,      // Enable Ping-Pong classification 
                        rdis | 0x11, // Type 2 Icut
                        0xC0         // Type 2 Ilim
                      };
    
    API_I2C_DMA_set_regs(reg, reg_val, address, 20);
}

void ltc4266_get_config(uint8_t address)
{
  uint8_t reg[] = {
    supevn_cor,
    statpwr,
    statp1,
    statp2,
    statp3,
    statp4,
    opmd,
    ip1lsb,
    ip1msb,
    ip2lsb,
    ip2msb,
    ip3lsb,
    ip3msb,
    ip4lsb,
    ip4msb,
    vp1lsb,
    vp1msb,
    vp2lsb,
    vp2msb,
    vp3lsb,
    vp3msb,
    vp4lsb,
    vp4msb,
  };

  API_I2C_DMA_get_regs(reg, address, 23);
}

void ltc4266_set_config(uint8_t address, ltc4266_config_t* ltc4266_config)
{
  if (ltc4266_config[0].output_enable == ON)       { API_setDO(port_pin_nshdn1, 1); }
  else if (ltc4266_config[0].output_enable == OFF) { API_setDO(port_pin_nshdn1, 0); }
  //else;

  if (ltc4266_config[1].output_enable == ON) { API_setDO(port_pin_nshdn2, 1); }
  else if (ltc4266_config[1].output_enable == OFF) { API_setDO(port_pin_nshdn2, 0); }
  //else;

  if (ltc4266_config[2].output_enable == ON) { API_setDO(port_pin_nshdn3, 1); }
  else if (ltc4266_config[2].output_enable == OFF) { API_setDO(port_pin_nshdn3, 0); }
  //else;

  if (ltc4266_config[3].output_enable == ON) { API_setDO(port_pin_nshdn4, 1); }
  else if (ltc4266_config[3].output_enable == OFF) { API_setDO(port_pin_nshdn4, 0); }
  //else;

  uint8_t user_opmd = 0;
  uint8_t user_cut1 = rdis;
  uint8_t user_cut2 = rdis;
  uint8_t user_cut3 = rdis;
  uint8_t user_cut4 = rdis;
  uint8_t user_lim1;
  uint8_t user_lim2;
  uint8_t user_lim3;
  uint8_t user_lim4;
  uint8_t user_pwrpb;

  if (ltc4266_config[0].output_mode == PASSIVE) { user_opmd |= opmd1_manual; }
  else if (ltc4266_config[0].output_mode == TYPE_1) { user_opmd |= opmd1_semiauto; }
  else if (ltc4266_config[0].output_mode == TYPE_2) { user_opmd |= opmd1_semiauto; }
  else if (ltc4266_config[0].output_mode == LTPOE_PP) { user_opmd |= opmd1_semiauto; }
  //else;

  if (ltc4266_config[1].output_mode == PASSIVE) { user_opmd |= opmd2_manual; }
  else if (ltc4266_config[1].output_mode == TYPE_1) { user_opmd |= opmd2_semiauto; }
  else if (ltc4266_config[1].output_mode == TYPE_2) { user_opmd |= opmd2_semiauto; }
  else if (ltc4266_config[1].output_mode == LTPOE_PP) { user_opmd |= opmd2_semiauto; }
  //else; 

  if (ltc4266_config[2].output_mode == PASSIVE) { user_opmd |= opmd3_manual; }
  else if (ltc4266_config[2].output_mode == TYPE_1) { user_opmd |= opmd3_semiauto; }
  else if (ltc4266_config[2].output_mode == TYPE_2) { user_opmd |= opmd3_semiauto; }
  else if (ltc4266_config[2].output_mode == LTPOE_PP) { user_opmd |= opmd3_semiauto; }
  //else;

  if (ltc4266_config[3].output_mode == PASSIVE) { user_opmd |= opmd4_manual; }
  else if (ltc4266_config[3].output_mode == TYPE_1) { user_opmd |= opmd4_semiauto; }
  else if (ltc4266_config[3].output_mode == TYPE_2) { user_opmd |= opmd4_semiauto; }
  else if (ltc4266_config[3].output_mode == LTPOE_PP) { user_opmd |= opmd4_semiauto; }
  //else;


  if (ltc4266_config[0].output_mode == PASSIVE) { user_cut1 |= ((uint8_t)(ltc4266_config[0].current_limit*900.0/37.5) & 0x3F); }
  else if (ltc4266_config[0].output_mode == TYPE_1) { user_cut1 |= ((uint8_t)(ltc4266_config[0].current_limit*900.0 / 37.5) & 0x0A); }
  else if (ltc4266_config[0].output_mode == TYPE_2) { user_cut1 |= ((uint8_t)(ltc4266_config[0].current_limit*900.0 / 37.5) & 0x11); }
  else if (ltc4266_config[0].output_mode == LTPOE_PP) { user_cut1 |= ((uint8_t)(ltc4266_config[0].current_limit*900.0 / 37.5) & 0x3F); }
  //else;

  if (ltc4266_config[1].output_mode == PASSIVE) { user_cut2 |= ((uint8_t)(ltc4266_config[1].current_limit*900.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[1].output_mode == TYPE_1) { user_cut2 |= ((uint8_t)(ltc4266_config[1].current_limit*900.0 / 37.5) & 0x0A); }
  else if (ltc4266_config[1].output_mode == TYPE_2) { user_cut2 |= ((uint8_t)(ltc4266_config[1].current_limit*900.0 / 37.5) & 0x11); }
  else if (ltc4266_config[1].output_mode == LTPOE_PP) { user_cut2 |= ((uint8_t)(ltc4266_config[1].current_limit*900.0 / 37.5) & 0x3F); }
  //else;

  if (ltc4266_config[2].output_mode == PASSIVE) { user_cut3 |= ((uint8_t)(ltc4266_config[2].current_limit*900.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[2].output_mode == TYPE_1) { user_cut3 |= ((uint8_t)(ltc4266_config[2].current_limit*900.0 / 37.5) & 0x0A); }
  else if (ltc4266_config[2].output_mode == TYPE_2) { user_cut3 |= ((uint8_t)(ltc4266_config[2].current_limit*900.0 / 37.5) & 0x11); }
  else if (ltc4266_config[2].output_mode == LTPOE_PP) { user_cut3 |= ((uint8_t)(ltc4266_config[2].current_limit*900.0 / 37.5) & 0x3F); }
  //else;

  if (ltc4266_config[3].output_mode == PASSIVE) { user_cut4 |= ((uint8_t)(ltc4266_config[3].current_limit*900.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[3].output_mode == TYPE_1) { user_cut4 |= ((uint8_t)(ltc4266_config[3].current_limit*900.0 / 37.5) & 0x0A); }
  else if (ltc4266_config[3].output_mode == TYPE_2) { user_cut4 |= ((uint8_t)(ltc4266_config[3].current_limit*900.0 / 37.5) & 0x11); }
  else if (ltc4266_config[3].output_mode == LTPOE_PP) { user_cut4 |= ((uint8_t)(ltc4266_config[3].current_limit*900.0 / 37.5) & 0x3F); }
  //else;

  if (ltc4266_config[0].output_mode == PASSIVE) { user_lim1 = ((uint8_t)(ltc4266_config[0].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[0].output_mode == TYPE_1) { user_lim1 = ((uint8_t)(ltc4266_config[0].current_limit*1000.0 / 37.5) & 0x80); }
  else if (ltc4266_config[0].output_mode == TYPE_2) { user_lim1 = ((uint8_t)(ltc4266_config[0].current_limit*1000.0 / 37.5) & 0xC0); }
  else if (ltc4266_config[0].output_mode == LTPOE_PP) { user_lim1 = ((uint8_t)(ltc4266_config[0].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[0].current_limit != 0) { user_lim1 = ((uint8_t)(ltc4266_config[0].current_limit*1000.0 / 37.5) & 0x3F); }

  if (ltc4266_config[1].output_mode == PASSIVE) { user_lim2 = ((uint8_t)(ltc4266_config[1].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[1].output_mode == TYPE_1) { user_lim2 = ((uint8_t)(ltc4266_config[1].current_limit*1000.0 / 37.5) & 0x80); }
  else if (ltc4266_config[1].output_mode == TYPE_2) { user_lim2 = ((uint8_t)(ltc4266_config[1].current_limit*1000.0 / 37.5) & 0xC0); }
  else if (ltc4266_config[1].output_mode == LTPOE_PP) { user_lim2 = ((uint8_t)(ltc4266_config[1].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[1].current_limit != 0) { user_lim2 = ((uint8_t)(ltc4266_config[1].current_limit*1000.0 / 37.5) & 0x3F); }

  if (ltc4266_config[2].output_mode == PASSIVE) { user_lim3 = ((uint8_t)(ltc4266_config[2].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[2].output_mode == TYPE_1) { user_lim3 = ((uint8_t)(ltc4266_config[2].current_limit*1000.0 / 37.5) & 0x80); }
  else if (ltc4266_config[2].output_mode == TYPE_2) { user_lim3 = ((uint8_t)(ltc4266_config[2].current_limit*1000.0 / 37.5) & 0xC0); }
  else if (ltc4266_config[2].output_mode == LTPOE_PP) { user_lim3 = ((uint8_t)(ltc4266_config[2].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[2].current_limit != 0) { user_lim3 = ((uint8_t)(ltc4266_config[2].current_limit*1000.0 / 37.5) & 0x3F); }

  if (ltc4266_config[3].output_mode == PASSIVE) { user_lim4 = ((uint8_t)(ltc4266_config[3].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[3].output_mode == TYPE_1) { user_lim4 = ((uint8_t)(ltc4266_config[3].current_limit*1000.0 / 37.5) & 0x80); }
  else if (ltc4266_config[3].output_mode == TYPE_2) { user_lim4 = ((uint8_t)(ltc4266_config[3].current_limit*1000.0 / 37.5) & 0xC0); }
  else if (ltc4266_config[3].output_mode == LTPOE_PP) { user_lim4 = ((uint8_t)(ltc4266_config[3].current_limit*1000.0 / 37.5) & 0x3F); }
  else if (ltc4266_config[3].current_limit != 0) { user_lim4 = ((uint8_t)(ltc4266_config[3].current_limit*1000.0 / 37.5) & 0x3F); }

  if (ltc4266_config[0].output_mode == PASSIVE) { user_pwrpb |= on1; }
  //else;

  if (ltc4266_config[1].output_mode == PASSIVE) { user_pwrpb |= on2; }
  //else;

  if (ltc4266_config[2].output_mode == PASSIVE) { user_pwrpb |= on3; }
  //else;

  if (ltc4266_config[3].output_mode == PASSIVE) { user_pwrpb |= on4; }
  //else;

  uint8_t reg[] = {
    opmd,
    cut1,
    cut2,
    cut3,
    cut4,
    lim1,
    lim2,
    lim3,
    lim4,
    pwrpb
  };

  uint8_t reg_val[] = { user_opmd,
  user_cut1,
  user_cut2,
  user_cut3,
  user_cut4,
  user_lim1,
  user_lim2,
  user_lim3,
  user_lim4,
  user_pwrpb
  };

  API_I2C_DMA_set_regs(reg, reg_val, address, 10);
}

#endif // __USING_LTC4266
