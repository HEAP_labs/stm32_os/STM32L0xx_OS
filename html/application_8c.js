var application_8c =
[
    [ "crc16_ok", "application_8c.html#a985b46cca3411314489e668a89ccce54", null ],
    [ "error_handler", "application_8c.html#a2080215bcaaf67f6e9b87b06c6f32a47", null ],
    [ "get_error_code", "application_8c.html#ad85f4406496b593af05dbaed475462e9", null ],
    [ "get_plate_state", "application_8c.html#a36dcfc6fee22b0974badca8dff09e55d", null ],
    [ "gpio_config", "application_8c.html#a16ab82c8a753266e4ffede309ca4259c", null ],
    [ "is_last_board", "application_8c.html#af0c030ef2e6fcfbc87477d3f0ff9450a", null ],
    [ "lpuart_received_handler", "application_8c.html#a9cbc3d9fe770f18edadfac923e609164", null ],
    [ "measure_state_machine", "application_8c.html#a4c6bb82dbda8b2832ea1da7405a140d1", null ],
    [ "set_led", "application_8c.html#a972149ddf8c5a245a0457877732117bc", null ],
    [ "startup_sequence", "application_8c.html#a13d9e1c12926c3cf40995d4b5baafd1a", null ],
    [ "usart_received_handler", "application_8c.html#aeb51d77ac5f7ea3f81d0ba5adb585b0f", null ],
    [ "g_b_is_last_board", "application_8c.html#abd5a655d2ae02e2266a2215f3b2e64d1", null ],
    [ "g_b_measurement_running", "application_8c.html#a817070dd4b0bdeb10561d20e8da689cb", null ],
    [ "g_b_start_measurement", "application_8c.html#a7882ae0be5c0e7308062c842d3fe7c88", null ],
    [ "g_board_index", "application_8c.html#ad3cadd4aa11ddf117d1ede07739be9b4", null ],
    [ "g_measured_data", "application_8c.html#a27a5c8ab16e81cd991cb5ca2380eedf8", null ]
];