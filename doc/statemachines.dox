/**

\page State-Machines
# State-Machines #

## Measure-State-Machine ##

\dot
digraph measure_state_machine {
    wait -> ref_measure;
    ref_measure -> measure;
    measure -> ref_measure;
    measure -> send_data;
    measure -> wait;
    send_data-> wait;
}
\enddot

The measure state machine is called by the USART received handler and starts in the wait state. 
Once the input i_start_measure is set, the state machine goes into the ref_measure state and starts the reference measurement of the sensors. 
All infrared - emitters are turned off. After the reference measurement, the state machine continues in the measure state and turns the ir - emitters on. 
After the switch - on time (approximately 200us) of the photo - transistor, the measurement starts. 
Immediately after the measurement, the state-machine goes into the send_data state, if the board is the last board in the daisy chain and sends the measured data over USART. 
After this procedure, the state machine turns into the wait - state.
@see measure_state_machine

## USART-Received-State-Machine ##

\dot
digraph usart_state_machine {
    new_frame -> wait_for_crc;
    wait_for_crc -> wait_for_crc;
    wait_for_crc -> check_crc;
    check_crc -> send_error;
    check_crc -> start_tc;
    start_tc -> new_frame;
    start_tc -> send_index;
    send_index -> new_frame;
    send_error -> new_frame;
}
\enddot

The state machine starts in the new_frame state if a new USART frame is received. The first received byte contains the index and the status, the state machine is in the wait_for_crc state now.
After the first received byte, a two byte CRC16 (CCITT) is received and the state machine turns into the check_crc state, where the CRC is checked. After the CR-Check the state machine goes into the send_error state, if the check fails. 
A valid CRC in the frame turns the state machine in the start_tc state, where the taskclass is started and the Measure-State-Machine is called. If the current board is the last board, the state machine turn into the new_frame state. 
If not, the index is transmitted by the LPUART and the state is send_index. After the send_index state or the send_error, the state machine returns to the new_frame state. 
@see usart_received_handler

## LPUART Received State Machine ##

\dot
digraph lpuart_state_machine {
    new_frame -> send_error;
    new_frame -> wait_for_last_index;
    wait_for_last_index -> wait_for_last_index;
    wait_for_last_index -> wait_for_crc;
    wait_for_crc -> check_crc;
    check_crc -> send_data;
    check_crc -> send_error;
    send_data -> new_frame;
    send_error -> new_frame;
}
\enddot
The state machine starts in the new_frame, if a LPUART frame is received. If the received status in the index-packet is an error, the state - machine goes into the send_error state, else the received packet is valid and the wait_for_last_index state turns into the current state.
In the wait_for_last_index state, the state machine waits for the index-byte, where the status is the last-board-status. After all packets received, the state machine turns into the check_crc state and checks the current received CRC16. If the CRC is valid, the data is sent over the USART, incuding the index, status and the measured data of the current board. 
A failed CRC-Check forces the state machine into the send_error state and an error is sent. Then the state-machine turns back into the new_frame state.
@see lpuart_received_handler

\copyright Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
*/

