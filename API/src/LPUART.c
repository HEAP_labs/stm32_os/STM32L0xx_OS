/**
 * @file    ./src/LPUART.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the LPTIMER module
 */

#include "PerUsings.h"
#ifdef __USING_LPUART

/* Includes ------------------------------------------------------------------*/
#include "LPUART.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void API_LPUART_DMA_init(uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t priority);
void API_LPUART_init(API_GPIO_type_t * lpuart_port_pin_rx,
        API_GPIO_alternative_function_t lpuart_alternative_function_rx,
        API_GPIO_type_t * lpuart_port_pin_tx,
        API_GPIO_alternative_function_t lpuart_alternative_function_tx,
        uint32_t baudrate);
void API_LPUART_DMA_send(uint8_t tx_length);
void API_LPUART_DMA_receive(uint16_t rx_length);

/* Private functions ---------------------------------------------------------*/

/*!
 * @brief Initialize DMA for LPUART
 */
void API_LPUART_DMA_init(uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t priority) {
    /* Enable the peripheral clock DMA1 */
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;

    /* DMA1 Channel2 LPUART1_TX config */
    /* Map LPUART1_TX DMA channel */
    DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C2S) | (5 << (1 * 4));
    /* Peripheral address */
    DMA1_Channel2->CPAR = (uint32_t) &(LPUART1->TDR);
    /* Memory address */
    DMA1_Channel2->CMAR = (uint32_t) p_tx_data;
    /* Memory increment */
    /* Memory to peripheral */
    /* 8-bit transfer */
    /* Transfer complete IT */
    DMA1_Channel2->CCR = DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE;
    /* DMA1 Channel3 LPUART1_RX config */
    /* Map LPUART1_RX DMA channel */
    DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C3S) | (5 << (2 * 4));
    /* Peripheral address */
    DMA1_Channel3->CPAR = (uint32_t) &(LPUART1->RDR);
    /* Memory address */
    DMA1_Channel3->CMAR = (uint32_t) p_rx_data;
    /* Memory increment */
    /* Peripheral to memory*/
    /* 8-bit transfer */
    /* Transfer complete IT */
    DMA1_Channel3->CCR = DMA_CCR_MINC | DMA_CCR_TCIE | DMA_CCR_EN;

    /* Configure IT */
    /* Set priority for DMA1_Channel2_3_IRQn */
    NVIC_SetPriority(DMA1_Channel2_3_IRQn, priority);
}

/*!
 * @brief Initialize LPUART
 * @param lpuart_port_pin_rx GPIO Instance of the RX-Pin
 * @param lpuart_alternative_function_rx Alternative-Function Number of the RX-Pin
 * @param lpuart_port_pin_tx GPIO Instance of the TX-Pin
 * @param lpuart_alternative_function_tx Alternative-Function Number of the TX-Pin
 * @param baudrate Baudrate used for the Low-Power UART
 * @param handler function pointer to the received-handler
 */
void API_LPUART_init(API_GPIO_type_t * lpuart_port_pin_rx,
    API_GPIO_alternative_function_t lpuart_alternative_function_rx,
    API_GPIO_type_t * lpuart_port_pin_tx,
    API_GPIO_alternative_function_t lpuart_alternative_function_tx,
    uint32_t baudrate) {
    
  /* Initialize GPIO for the LPUART operation */
  lpuart_port_pin_rx->GPIOX_Mode = ALTERNATIVE_FUNCTION;
  lpuart_port_pin_rx->GPIOX_Type = PUSH_PULL;
  lpuart_port_pin_rx->GPIOX_AlternativeFunction =
          lpuart_alternative_function_rx;
  lpuart_port_pin_tx->GPIOX_Mode = ALTERNATIVE_FUNCTION;
  lpuart_port_pin_tx->GPIOX_Type = PUSH_PULL;
  lpuart_port_pin_tx->GPIOX_AlternativeFunction =
          lpuart_alternative_function_tx;
  API_GPIO_init(lpuart_port_pin_rx);
  API_GPIO_init(lpuart_port_pin_tx);
  /* Enable power interface clock */
  RCC->APB1ENR |= (RCC_APB1ENR_PWREN);
  /* Disable back up protection register to allow the access to the RTC clock domain */
  PWR->CR |= PWR_CR_DBP;
  /* Enable back up protection register to allow the access to the RTC clock domain */
  PWR->CR &= ~ PWR_CR_DBP;
  /* HSI16 mapped on LPUART => 16Mhz Clock for LPUART */
  RCC->CCIPR = (RCC->CCIPR & ~RCC_CCIPR_LPUART1SEL) | RCC_CCIPR_LPUART1SEL_1;
  /* Enable the peripheral clock LPUART */
  RCC->APB1ENR |= RCC_APB1ENR_LPUART1EN;
  /* Calculate the baudrate */
  LPUART1->BRR = (uint32_t) (256 * SystemCoreClock / (baudrate));
  /* 8 data bit, 1 start bit, 1 stop bit, no parity */
  LPUART1->CR1 = USART_CR1_RE | USART_CR1_TE | USART_CR1_UE;
}

/*!
* @brief Send function for the LPUART using DMA
* @param tx_length length of the buffer in bytes
*/
void API_LPUART_DMA_send(uint8_t tx_length) {
    /* start 8-bit transmission with DMA */
  /* disable DMA Channel 2 */
  DMA1_Channel2->CCR &= ~ DMA_CCR_EN;
  /* size of the buffer */
  DMA1_Channel2->CNDTR = tx_length;
  /* Enable DMA for Transmitter */
  LPUART1->CR3 |= USART_CR3_DMAT;
  /* Enable DMA1_Channel2_3_IRQn */
  NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
  /* enable DMA Channel 2 */
  DMA1_Channel2->CCR |= DMA_CCR_EN;
}

/*!
* @brief Receive LPUART data using DMA
* @param rx_length length of the buffer in bytes
*/
void API_LPUART_DMA_receive(uint16_t rx_length)
{
  /* polling idle frame Transmission */
  while (DMA1_Channel3->CNDTR != 0) {
  }

  /* start 8-bit transmission with DMA */
  /* disable DMA Channel 3 */
  DMA1_Channel3->CCR &= ~DMA_CCR_EN;
  /* size of the buffer */
  DMA1_Channel3->CNDTR = rx_length;
  /* Enable DMA for Receiver */
  LPUART1->CR3 |= USART_CR3_DMAR;
  /* Enable DMA1_Channel2_3_IRQn */
  NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
  /* enable DMA Channel 3 */
  DMA1_Channel3->CCR |= DMA_CCR_EN;
}

#endif // __USING_LPUART
