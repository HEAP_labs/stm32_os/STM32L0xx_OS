/**
 * @file    ./API/inc/ADC.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the ADC.c module
 */

#ifndef __ADC_H
#define __ADC_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
extern double g_adc_ref_mv;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_ADC_init(void);
extern uint32_t API_ADC_get_voltage(API_GPIO_type_t* adc_pin);

#endif // __ADC_H

