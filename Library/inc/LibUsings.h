/**
 * @file    ./Library/inc/LibUsings.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file to define which Libraries are used
 */

#ifndef _LIBUSINGS_H
#define _LIBUSINGS_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

//#define __USING_CRC16
//#define __USING_LTC4266

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // _LIBUSINGS_H
