/**
 * @file    ./API/inc/LPUART.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the LPUART.c module
 */

#ifndef __PERUSINGS_H
#define __PERUSINGS_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define __USING_GPIO
//#define __USING_ADC
#define __USING_LPTIMER
//#define __USING_WWDG
//#define __USING_USART
//#define __USING_LPUART
//#define __USING_I2C
//#define __USING_EXTI

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __PERUSINGS_H

