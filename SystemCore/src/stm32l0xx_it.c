/**
  ******************************************************************************
  * @file    SystemCore/src/stm32l0xx_it.c
  * @author  MCD Application Team
  * @version V1.5.0
  * @date    06-March-2015
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_it.h"

/** @addtogroup STM32F4xx_StdPeriph_Examples
  * @{
  */

/** @addtogroup SysTick_Example
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief   This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}


/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{

}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f40xx.s/startup_stm32f427x.s/startup_stm32f429x.s).    */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @brief   This function handles DMA1 channel 4 and 5 interrupt request.
  * @param  None
  * @retval None
  */
void DMA1_Channel4_5_6_7_IRQHandler(void)
{
#ifdef __USING_I2C
  /* I2C */
  if ((DMA1->ISR & DMA_ISR_TCIF4) == DMA_ISR_TCIF4)
  {
    DMA1->IFCR |= DMA_IFCR_CTCIF4; /* Clear TC flag */
  }
  if ((DMA1->ISR & DMA_ISR_TCIF5) == DMA_ISR_TCIF5)
  {
    DMA1->IFCR |= DMA_IFCR_CTCIF5; /* Clear TC flag */
        
    //reg_val_ready
  }
#endif // __USING_I2C
  
#ifdef __USING_USART
  //TX
  if ((DMA1->ISR & DMA_ISR_TCIF4) == DMA_ISR_TCIF4) {
    /* Clear TC flag */
    DMA1->IFCR = DMA_IFCR_CTCIF4;
  }
  //RX
  if ((DMA1->ISR & DMA_ISR_TCIF5) == DMA_ISR_TCIF5) {
    /* Clear TC flag */
    DMA1->IFCR = DMA_IFCR_CTCIF5;
  }
#endif // __USING_USART
}

#ifdef __USING_LPUART
/*!
* @brief IRQ Handler, called when a DMA transfer is complete (RX and TX)
*/
void DMA1_Channel2_3_IRQHandler(void) {
  // TX
  if ((DMA1->ISR & DMA_ISR_TCIF2) == DMA_ISR_TCIF2) {
    /* Clear TC flag */
    DMA1->IFCR = DMA_IFCR_CTCIF2;
  }
  //RX
  else if ((DMA1->ISR & DMA_ISR_TCIF3) == DMA_ISR_TCIF3) {
    /* Clear TC flag */
    DMA1->IFCR = DMA_IFCR_CTCIF3;
  }
}
#endif // __USING_LPUART

#ifdef __USING_EXTI
void EXTI0_1_IRQHandler(void)
{
  if ((EXTI->PR & EXTI_PR_PR0) != 0)  /* Check line 0 has triggered the IT */
  {
    EXTI->PR = EXTI_PR_PR0; /* Clear the pending bit */

  }
  else if ((EXTI->PR & EXTI_PR_PR1) != 0)  /* Check line 1 has triggered the IT */
  {
    EXTI->PR = EXTI_PR_PR1; /* Clear the pending bit */

  }
}

void EXTI2_3_IRQHandler(void)
{
  if ((EXTI->PR & EXTI_PR_PR2) != 0)  /* Check line 2 has triggered the IT */
  {
    EXTI->PR = EXTI_PR_PR2; /* Clear the pending bit */

  }
  else if ((EXTI->PR & EXTI_PR_PR3) != 0)  /* Check line 3 has triggered the IT */
  {
    EXTI->PR = EXTI_PR_PR3; /* Clear the pending bit */

  }
}

void EXTI4_15_IRQHandler(void)
{
  if ((EXTI->PR & EXTI_PR_PR4) != 0)  /* Check line 4 has triggered the IT */
  {
    EXTI->PR = EXTI_PR_PR4; /* Clear the pending bit */

  }
}

#endif // __USING_EXTI



/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
