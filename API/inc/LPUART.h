/**
 * @file    ./API/inc/LPUART.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the LPUART.c module
 */

#ifndef __LPUART_H
#define __LPUART_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_LPUART_DMA_init(uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t priority);
extern void API_LPUART_init(API_GPIO_type_t * lpuart_port_pin_rx,
  API_GPIO_alternative_function_t lpuart_alternative_function_rx,
  API_GPIO_type_t * lpuart_port_pin_tx,
  API_GPIO_alternative_function_t lpuart_alternative_function_tx,
  uint32_t baudrate);
extern void API_LPUART_DMA_send(uint8_t tx_length);
extern void API_LPUART_DMA_receive(uint16_t rx_length);

#endif /* __LPUART_H */

