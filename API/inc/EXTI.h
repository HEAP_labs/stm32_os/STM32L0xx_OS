/**
 * @file    ./API/inc/EXTI.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file for the EXTI.c module
 */

#ifndef __EXTI_H
#define __EXTI_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_EXTI_GPIO_init(API_GPIO_type_t* portpin, uint8_t rising_edge_trigger, uint8_t falling_edge_trigger, uint32_t priority);

#endif // __EXTI_H

