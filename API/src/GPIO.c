/**
 * @file    ./API/src/GPIO.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the GPIO module
 */

#include "PerUsings.h"
#ifdef __USING_GPIO

/* Includes ------------------------------------------------------------------*/
#include "GPIO.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define SET_BITS(REG,STARTBIT,LENGTH,VALUE) \
                ((REG) = (((REG) & ~(((1 << (LENGTH))-1) << (STARTBIT))) \
                                | ((VALUE) << (STARTBIT))))
    
/* Private variables ---------------------------------------------------------*/
API_GPIO_type_t PA0 = { GPIOA, 0, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA1 = { GPIOA, 1, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA2 = { GPIOA, 2, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA3 = { GPIOA, 3, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA4 = { GPIOA, 4, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA5 = { GPIOA, 5, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA6 = { GPIOA, 6, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA7 = { GPIOA, 7, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA8 = { GPIOA, 8, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA9 = { GPIOA, 9, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA10 = { GPIOA, 10, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA11 = { GPIOA, 11, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA12 = { GPIOA, 12, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA13 = { GPIOA, 13, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA14 = { GPIOA, 14, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PA15 = { GPIOA, 15, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };

API_GPIO_type_t PB0 = { GPIOB, 0, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PB1 = { GPIOB, 1, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PB3 = { GPIOB, 3, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PB4 = { GPIOB, 4, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PB5 = { GPIOB, 5, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PB6 = { GPIOB, 6, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PB7 = { GPIOB, 7, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PB9 = { GPIOB, 9, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };

API_GPIO_type_t PC14 = { GPIOC, 14, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
API_GPIO_type_t PC15 = { GPIOC, 15, INPUT, OPEN_DRAIN, GPIO_SPEED_10MHz, PULL_UP,
        ALTERNATIVE_FUNCTION_0, 0 };
/* Private function prototypes -----------------------------------------------*/
void API_GPIO_init(API_GPIO_type_t* API_GPIO_type);
void API_GPIO_deinit(API_GPIO_type_t* API_GPIO_type);
void API_setDO(API_GPIO_type_t * API_GPIO_type, uint8_t bit_val);
uint8_t API_getDO(API_GPIO_type_t * API_GPIO_type);
uint8_t API_getDI(API_GPIO_type_t * API_GPIO_type);

/* Private functions ---------------------------------------------------------*/

/*!
 * @brief Initialize a GPIO instance
 * @param API_GPIO_type Instance of the GPIO Pin
 */
void API_GPIO_init(API_GPIO_type_t* API_GPIO_type) {
    /* Enable the clock for the specified port */
    if (API_GPIO_type->GPIOX == GPIOA) {
        RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
    } else if (API_GPIO_type->GPIOX == GPIOB) {
        RCC->IOPENR |= RCC_IOPENR_GPIOBEN;
    } else if (API_GPIO_type->GPIOX == GPIOC) {
        RCC->IOPENR |= RCC_IOPENR_GPIOCEN;
    }

    /* Set the pin - mode in the MODER - register for the GPIO Instance */
    SET_BITS(API_GPIO_type->GPIOX->MODER, (API_GPIO_type->GPIOX_Pin) * 2, 2,
            API_GPIO_type->GPIOX_Mode);
    /* Set the pin - type in the OTYPER - register for the GPIO Instance */
    SET_BITS(API_GPIO_type->GPIOX->OTYPER, (API_GPIO_type->GPIOX_Pin) * 2, 2,
            API_GPIO_type->GPIOX_Type);
    /* Set the pin - speed in the OSPEEDR - register for the GPIO Instance */
    SET_BITS(API_GPIO_type->GPIOX->OSPEEDR, (API_GPIO_type->GPIOX_Pin) * 2, 2,
            API_GPIO_type->GPIOX_Speed);
    /* Set the pin - pull in the PUPDR - register for the GPIO Instance */
    SET_BITS(API_GPIO_type->GPIOX->PUPDR, (API_GPIO_type->GPIOX_Pin) * 2, 2,
            API_GPIO_type->GPIOX_Pull);

    /* Select the low (pin 0 to 7) or the high (pin 8 to 15) - register */
    if (API_GPIO_type->GPIOX_Pin < 8) {
        /* Select the Alternative Function in the low - register*/
        SET_BITS(API_GPIO_type->GPIOX->AFR[0], (API_GPIO_type->GPIOX_Pin) * 4, 4,
                API_GPIO_type->GPIOX_AlternativeFunction);
    } else if (API_GPIO_type->GPIOX_Pin > 7 && API_GPIO_type->GPIOX_Pin <= 15) {
        /* Select the Alternative Function in the high - register*/
        SET_BITS(API_GPIO_type->GPIOX->AFR[1], (API_GPIO_type->GPIOX_Pin - 8) * 4,
                4, API_GPIO_type->GPIOX_AlternativeFunction);
    }
}

/*!
 * @brief Disable the GPIO clock
 * @param API_GPIO_type  Instance of the GPIO Pin
 */
void API_GPIO_deinit(API_GPIO_type_t* API_GPIO_type) {
    /* Disable the Clock on the spcified port */
    if (API_GPIO_type->GPIOX == GPIOA) {
        RCC->IOPENR &= ~RCC_IOPENR_GPIOAEN;
    } else if (API_GPIO_type->GPIOX == GPIOB) {
        RCC->IOPENR &= ~RCC_IOPENR_GPIOBEN;
    } else if (API_GPIO_type->GPIOX == GPIOC) {
        RCC->IOPENR &= ~RCC_IOPENR_GPIOCEN;
    }
}

/*!
 * @brief Set the output of a GPIO pin
 * @param API_GPIO_type  Instance of the GPIO Pin
 * @param bit_val Value 0...Low >0...High
 */
void API_setDO(API_GPIO_type_t * API_GPIO_type, uint8_t bit_val) {
    if (bit_val)
        /* set the pin in the Bit-Set-Register */
        API_GPIO_type->GPIOX->BSRR = (1 << API_GPIO_type->GPIOX_Pin);
    else
        /* set the pin in the Bit-Reset-Register */
        API_GPIO_type->GPIOX->BRR = (1 << API_GPIO_type->GPIOX_Pin);
}

uint8_t API_getDO(API_GPIO_type_t * API_GPIO_type) {
  return (API_GPIO_type->GPIOX->ODR >> API_GPIO_type->GPIOX_Pin) & 0x01;
}

/*!
 * @brief Get the input value of a GPIO pin
 * @param API_GPIO_type  Instance of the GPIO Pin
 * @retval 0 Low
 * @retval 1 High
 */
uint8_t API_getDI(API_GPIO_type_t * API_GPIO_type) {
    if ((API_GPIO_type->GPIOX->IDR & (1 << API_GPIO_type->GPIOX_Pin)))
        return 1;
    else
        return 0;
}

#endif // __USING_GPIO
