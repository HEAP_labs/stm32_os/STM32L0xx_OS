var group__STM32L0xx__System__Private__Variables =
[
    [ "AHBPrescTable", "group__STM32L0xx__System__Private__Variables.html#ga6e1d9cd666f0eacbfde31e9932a93466", null ],
    [ "PLLMulTable", "group__STM32L0xx__System__Private__Variables.html#gadab2d89c9fe6053f421278d154dcfb9d", null ],
    [ "SystemCoreClock", "group__STM32L0xx__System__Private__Variables.html#gaa3cd3e43291e81e795d642b79b6088e6", null ]
];