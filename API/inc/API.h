/**
 * @file    ./API/inc/API.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file to include APIs defiened in PerUsings.h
 */

#ifndef _API_H
#define _API_H

/* Exported constants --------------------------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "PerUsings.h"

#ifdef __USING_GPIO
  #include "GPIO.h"	  
#endif // __USING_GPIO

#ifdef __USING_ADC
  #include "ADC.h"
#endif //__USING_ADC

#ifdef __USING_LPTIMER
  #include "LPTIMER.h"
#endif // __USING_LPTIMER

#ifdef __USING_WWDG
  #include "WWDG.h"
#endif // __USING_WWDG

#ifdef __USING_USART
  #include "USART.h"
#endif // __USING_USART

#ifdef __USING_LPUART
  #include "LPUART.h"
#endif // __USING_LPUART

#ifdef __USING_I2C
  #include "I2C.h"
#endif // __USING_I2C

#ifdef __USING_EXTI
  #include "EXTI.h"
#endif // __USING_EXTI

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* _API_H */
