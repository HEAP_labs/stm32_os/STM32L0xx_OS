/**
 * @file    ./SystemCore/inc/main.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the main module
 */

#ifndef _MAIN_H
#define _MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "API.h"
#include "Library.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#ifdef __USING_WWDG
    #define API_WWDG_disable()    API_WWDG_disable()
    #define API_WWDG_heartbeat()  API_WWDG_heartbeat()
    #define API_WWDG_enable()     API_WWDG_enable()
#else
    #define API_WWDG_disable()
    #define API_WWDG_heartbeat()
    #define API_WWDG_enable()
#endif

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* _MAIN_H */
