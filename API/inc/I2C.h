/**
 * @file    ./API/inc/GPIO.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the I2C.c module
 */

#ifndef __I2C_H
#define __I2C_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
extern uint8_t i2c_receive_buffer[];
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_I2C_init(API_GPIO_type_t* i2c_port_pin_sda,
                         API_GPIO_alternative_function_t i2c_alternative_function_sda,
                         API_GPIO_type_t* i2c_port_pin_scl,
                         API_GPIO_alternative_function_t i2c_alternative_function_scl);
extern void API_I2C_DMA_init(void);
extern void API_I2C_DMA_set_reg(uint8_t reg, uint8_t reg_val, uint8_t slave_address);
extern void API_I2C_DMA_set_regs(uint8_t* reg, uint8_t* reg_val, uint8_t slave_address, uint8_t reg_count);
extern void API_I2C_DMA_get_reg(uint8_t reg, uint8_t slave_address);
extern void API_I2C_DMA_get_regs(uint8_t* reg, uint8_t slave_address, uint8_t reg_count);

#endif /* __I2C_H */
