var system__stm32l0xx_8h =
[
    [ "SystemCoreClockUpdate", "group__STM32L0xx__System__Exported__Functions.html#gae0c36a9591fe6e9c45ecb21a794f0f0f", null ],
    [ "SystemInit", "group__STM32L0xx__System__Exported__Functions.html#ga93f514700ccf00d08dbdcff7f1224eb2", null ],
    [ "AHBPrescTable", "group__STM32L0xx__System__Exported__types.html#ga6e1d9cd666f0eacbfde31e9932a93466", null ],
    [ "PLLMulTable", "group__STM32L0xx__System__Exported__types.html#gadab2d89c9fe6053f421278d154dcfb9d", null ],
    [ "SystemCoreClock", "group__STM32L0xx__System__Exported__types.html#gaa3cd3e43291e81e795d642b79b6088e6", null ]
];