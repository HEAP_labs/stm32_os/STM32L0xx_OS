/**
 * @file    ./API/src/WWDG.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Implementation of the Window Watchdog Timer module
 */

#include "PerUsings.h"
#ifdef __USING_WWDG

/* Includes ------------------------------------------------------------------*/
#include "WWDG.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/*! @brief function pointer for watchdog handler */
static void (*p_wwdg_handler)(void);

/* Private function prototypes -----------------------------------------------*/
void API_WWDG_init(void (*handler)(void));
void API_WWDG_heartbeat(void);
void API_WWDG_enable(void);
void API_WWDG_disable(void);
void WWDG_IRQHandler(void);

/* Private functions ---------------------------------------------------------*/

/*!
 * @brief Initialize the Watchdog for a reset-time of 130ms
 * @param handler function pointer to watchdog-handler
 */
void API_WWDG_init(void (*handler)(void)) {
    /* store the watchdog-handler */
    p_wwdg_handler = handler;

#ifdef DEBUG_MODE
    /* Enable the peripheral clock of DBG register (uncomment for debug purpose) */
    RCC->APB2ENR |= RCC_APB2ENR_DBGMCUEN;
    DBGMCU->APB1FZ |= DBGMCU_APB1_FZ_DBG_WWDG_STOP; /* To be able to debug */
#endif

    /* Enable the peripheral clock WWDG */
    RCC->APB1ENR |= RCC_APB1ENR_WWDGEN;
    /* enable Early Wakeup Interrupt, divide clock by 8, set Window value to maximum */
    WWDG->CFR |= (WWDG_CFR_W | WWDG_CFR_EWI | WWDG_CFR_WDGTB);
    /* Set the counter-Register */
    API_WWDG_heartbeat();
    /* Enable the Watchdog */
    WWDG->CR |= WWDG_CR_WDGA;

    /* Configure IT */
    /* Set priority for WWDG_IRQn */
    NVIC_SetPriority(WWDG_IRQn, 0);
    /* Enable WWDG_IRQn */
    NVIC_EnableIRQ(WWDG_IRQn);
}

/*!
 * @brief resets the watchdog counter register for a reset-time of 130ms
 */
void API_WWDG_heartbeat(void) {
    WWDG->CR = 0x7F;
}

/*!
 * @brief enable the watchdog
 */
void API_WWDG_enable(void) {
    /* Enable the peripheral clock WWDG */
    RCC->APB1ENR |= RCC_APB1ENR_WWDGEN;
    /* enable the watchdog */
    WWDG->CR |= WWDG_CR_WDGA;
}

/*!
 * @brief disable the watchdog
 */
void API_WWDG_disable(void) {
    API_WWDG_heartbeat();
    /* Disable the peripheral clock WWDG */
    RCC->APB1ENR &= ~RCC_APB1ENR_WWDGEN;
}

/*!
 * @brief Interrupt handler for the watchdog
 */
void WWDG_IRQHandler(void) {
    /* clear the pending flag */
    WWDG->SR &= ~WWDG_SR_EWIF;
    /* call the watchdog-handler */
    (*p_wwdg_handler)();
}

#endif // __USING_WWDG
