/**
 * @file    ./API/inc/GPIO.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
 * @brief   Header file for the GPIO.c module
 */

#ifndef __GPIO_H
#define __GPIO_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"

/* Exported types ------------------------------------------------------------*/
/*! @brief enum for the Mode of a GPIO Port-Pin */
typedef enum {
    INPUT = 0, OUTPUT = 1, ALTERNATIVE_FUNCTION = 2, ANALOG = 3
} API_GPIO_mode_t;

/*! @brief enum for the type of a GPIO Port-Pin */
typedef enum {
    PUSH_PULL = 0, OPEN_DRAIN = 1
} API_GPIO_output_type_t;

/*! @brief enum for the speed of a GPIO Port-Pin */
typedef enum {
    GPIO_SPEED_400kHz = 0,
    GPIO_SPEED_2MHz = 1,
    GPIO_SPEED_10MHz = 2,
    GPIO_SPEED_40MHz = 3,
} API_GPIO_speed_t;

/*! @brief enum for the pull-method of a GPIO Port-Pin */
typedef enum {
    NO_PULL = 0, PULL_UP = 1, PULL_DOWN = 2
} API_GPIO_pull_t;

/*! @brief enum for the alternative-function of a GPIO Port-Pin */
typedef enum {
    ALTERNATIVE_FUNCTION_0 = 0,
    ALTERNATIVE_FUNCTION_1 = 1,
    ALTERNATIVE_FUNCTION_2 = 2,
    ALTERNATIVE_FUNCTION_3 = 3,
    ALTERNATIVE_FUNCTION_4 = 4,
    ALTERNATIVE_FUNCTION_5 = 5,
    ALTERNATIVE_FUNCTION_6 = 6,
    ALTERNATIVE_FUNCTION_7 = 7
} API_GPIO_alternative_function_t;

/*! @brief structure for a GPIO Port-Pin */
typedef struct API_GPIO_type_s {
    GPIO_TypeDef * GPIOX; /*! @brief GPIO - Typedef provided by ST */
    uint32_t GPIOX_Pin; /*! @brief Pin-Number of the Port-Pin */
    API_GPIO_mode_t GPIOX_Mode; /*! @brief Pin-Number of the Port-Pin */
    API_GPIO_output_type_t GPIOX_Type; /*! @brief Port-Pin-Type */
    API_GPIO_speed_t GPIOX_Speed; /*! @brief Port-Pin-Speed */
    API_GPIO_pull_t GPIOX_Pull; /*! @brief Port-Pin-Pull method */
    /*! @brief Port-Pin-Alternative-Function Number */
    API_GPIO_alternative_function_t GPIOX_AlternativeFunction;
    uint8_t GPIOX_ADC_Channel;
} API_GPIO_type_t;

extern API_GPIO_type_t PA0;
extern API_GPIO_type_t PA1;
extern API_GPIO_type_t PA2;
extern API_GPIO_type_t PA3;
extern API_GPIO_type_t PA4;
extern API_GPIO_type_t PA5;
extern API_GPIO_type_t PA6;
extern API_GPIO_type_t PA7;
extern API_GPIO_type_t PA8;
extern API_GPIO_type_t PA9;
extern API_GPIO_type_t PA10;
extern API_GPIO_type_t PA11;
extern API_GPIO_type_t PA12;
extern API_GPIO_type_t PA13;
extern API_GPIO_type_t PA14;
extern API_GPIO_type_t PA15;

extern API_GPIO_type_t PB0;
extern API_GPIO_type_t PB1;
extern API_GPIO_type_t PB3;
extern API_GPIO_type_t PB4;
extern API_GPIO_type_t PB5;
extern API_GPIO_type_t PB6;
extern API_GPIO_type_t PB7;
extern API_GPIO_type_t PB9;

extern API_GPIO_type_t PC14;
extern API_GPIO_type_t PC15;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_GPIO_init(API_GPIO_type_t * API_GPIO_type);
extern void API_GPIO_deinit(API_GPIO_type_t * API_GPIO_type);
extern void API_setDO(API_GPIO_type_t * API_GPIO_type, uint8_t bit_val);
extern uint8_t API_getDO(API_GPIO_type_t * API_GPIO_type);
extern uint8_t API_getDI(API_GPIO_type_t * API_GPIO_type);

#endif /* __GPIO_H */

