/**
 * @file    ./API/src/EXTI.c
 * @author  Andreas Hirtenlehner
 * @brief   Implementation of external interrupts
 */

#include "PerUsings.h"
#ifdef __USING_EXTI

/* Includes ------------------------------------------------------------------*/
#include "EXTI.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void API_EXTI_GPIO_init(API_GPIO_type_t* portpin, uint8_t rising_edge_trigger, uint8_t falling_edge_trigger, uint32_t priority);

/* Private functions ---------------------------------------------------------*/

/*!
 * @brief Initialize a external interrupt from a portpin
 * @param portpin:              portpin for interrupt source
 * @param rising_edge_trigger:  0..disable, 1..enable
 * @param falling_edge_trigger: 0..disable, 1..enable
 * @param priority:             interrupt priority
 */
void API_EXTI_GPIO_init(API_GPIO_type_t* portpin, uint8_t rising_edge_trigger, uint8_t falling_edge_trigger, uint32_t priority)
{
  uint8_t  exticr_reg = portpin->GPIOX_Pin / 4;
  uint16_t exticr_extix = 0x000F << ((portpin->GPIOX_Pin % 4) * 4);
  uint8_t  exticr_port;
  
  if(portpin->GPIOX == GPIOA)      exticr_port = 0x00;
  else if(portpin->GPIOX == GPIOB) exticr_port = 0x01;
  else if(portpin->GPIOX == GPIOC) exticr_port = 0x02;
  
  portpin->GPIOX_Mode = INPUT;
  API_GPIO_init(portpin);
 
  SYSCFG->EXTICR[exticr_reg] &= ~exticr_extix;
  SYSCFG->EXTICR[exticr_reg] |= exticr_port << exticr_extix;
  EXTI->IMR |= 0x01 << portpin->GPIOX_Pin;

  if(rising_edge_trigger)  EXTI->RTSR |= 0x01 << portpin->GPIOX_Pin;
  if(falling_edge_trigger) EXTI->FTSR |= 0x01 << portpin->GPIOX_Pin;

  if(portpin->GPIOX_Pin == 0)      { NVIC_EnableIRQ(EXTI0_1_IRQn); NVIC_SetPriority(EXTI0_1_IRQn, priority);  }
  else if(portpin->GPIOX_Pin == 1) { NVIC_EnableIRQ(EXTI0_1_IRQn); NVIC_SetPriority(EXTI0_1_IRQn, priority);  }
  else if(portpin->GPIOX_Pin == 2) { NVIC_EnableIRQ(EXTI0_1_IRQn); NVIC_SetPriority(EXTI2_3_IRQn, priority);  }
  else if(portpin->GPIOX_Pin == 3) { NVIC_EnableIRQ(EXTI0_1_IRQn); NVIC_SetPriority(EXTI2_3_IRQn, priority);  }
  else                             { NVIC_EnableIRQ(EXTI0_1_IRQn); NVIC_SetPriority(EXTI4_15_IRQn, priority); }
}

#endif // __USING_EXTI
